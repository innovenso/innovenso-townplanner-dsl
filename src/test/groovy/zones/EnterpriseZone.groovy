package zones

import com.innovenso.townplan.dsl.Zone

import java.time.LocalDate


class EnterpriseZone extends Zone {
	EnterpriseZone() {
		concepts {
			timeMachine {
				pointInTime {
					title "the beginning of time"
					date LocalDate.of(2021,1,1)
					render false
				}

				pointInTime {
					title "the future"
					date LocalDate.of(2022, 1, 1)
					render true
				}

				today()
			}

			enterprise {
				key "hello"
				title "world"
				description "not bad"
			}

			principle {
				key "principle_chaos"
				title "Chaos"
				description "Chaos happens, design for it"
				type "design principle"
				enterprise "hello"
			}
		}
	}
}
