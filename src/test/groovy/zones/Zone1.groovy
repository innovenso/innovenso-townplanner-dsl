package zones

import com.innovenso.townplan.api.value.enums.ActorType
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock
import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.dsl.concepts.buildingblock.ArchitectureBuildingBlockConfigurer
import com.innovenso.townplan.dsl.concepts.capability.BusinessCapabilityConfigurer
import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer
import com.innovenso.townplan.dsl.concepts.system.ITSystemConfigurer

class Zone1 extends Zone {
	ITSystemConfigurer system123

	EnterpriseConfigurer innovenso

	BusinessCapabilityConfigurer productDevelopment

	ArchitectureBuildingBlockConfigurer buildingBlock

	Zone1() {
		concepts {

			innovenso = enterprise {
				title "Innovenso"
				description "The market leader in enterprise architecture as code"
			}

			productDevelopment = capability {
				title "Product Development"
				description "Developing products"
				enterprise this.innovenso

				capability {
					title "Requirements Management"
				}

				capability {
					title "Requirements Prioritization"
				}

				capability {
					title "Software Lifecycle Management"

					capability {
						title "Development"
					}

					capability {
						title "QA"
					}

					capability {
						title "Release"
					}
				}
			}

			capability {
				title "Sales and Marketing"
				enterprise this.innovenso

				capability {
					title "Sales"
				}

				capability {
					title "Marketing"
				}
			}

			buildingBlock = buildingBlock {
				title "BB"
				description "Some building block"
				enterprise this.innovenso

				realizes this.productDevelopment
			}

			system123 = system {
				title "System 1"
				description "a wonderful system"

				realizes this.buildingBlock
			}

			def theProject = project {
				title "the project"
				type "project"
				enterprise this.innovenso
			}

			actor {
				title "Jurgen Lust"
				description "Judge Jurgen"
				type ActorType.INDIVIDUAL
				enterprise this.innovenso

				hasBeenConsultedAbout theProject
			}
		}
	}
}
