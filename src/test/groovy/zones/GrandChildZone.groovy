package zones

import com.innovenso.townplan.dsl.Zone

class GrandChildZone extends Zone {
	GrandChildZone(ParentZone parentZone, ChildZone childZone) {
		concepts {
			buildingBlock {
				title "EA Tool"
				enterprise parentZone.theEnterprise

				realizes  childZone.managingEa
			}
		}
	}
}
