package zones

import com.innovenso.townplan.api.value.aspects.ArchitectureVerdictType
import com.innovenso.townplan.api.value.aspects.RequirementWeight
import com.innovenso.townplan.api.value.aspects.ScoreWeight
import com.innovenso.townplan.api.value.enums.ActorType
import com.innovenso.townplan.api.value.enums.ContainerType
import com.innovenso.townplan.api.value.enums.Criticality
import com.innovenso.townplan.api.value.enums.EntityType
import com.innovenso.townplan.api.value.enums.TechnologyRecommendation
import com.innovenso.townplan.api.value.enums.TechnologyType
import com.innovenso.townplan.api.value.it.decision.DecisionOptionVerdict
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.dsl.aspects.requirements.ConstraintAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.FunctionalRequirementAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.QualityAttributeRequirementAspectConfigurer
import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer
import com.innovenso.townplan.dsl.timemachine.TimeMachineConfigurer

import java.time.LocalDate


class CorrectZone extends Zone {

	CorrectZone() {
		concepts {
			timeMachine {
				pointInTime {
					title "the beginning of time"
					date 2021,1,1
					render false
				}

				pointInTime {
					title "the future"
					date 2022, 1, 1
					render true
				}

				today()
			}

			def hello = enterprise {
				key "hello"
				title "world"
				description "not bad"
			}

			principle {
				key "principle_chaos"
				title "Chaos"
				description "Chaos happens, design for it"
				type "design principle"
				enterprise hello.key
			}

			buildingBlock {
				key "cms"
				title "Content Management System"
				description "Will be Strapi no doubt"
				enterprise "hello"
			}

			actor {
				key "jurgen"
				title "Jurgen Lust"
				type ActorType.INDIVIDUAL
				enterprise "hello"

				hasBeenConsultedAbout {
					target "pi6"
					title "talked a lot about it"
				}
			}

			def virginie = actor {
				key "virginie"
				type ActorType.INDIVIDUAL
				enterprise "hello"
				title "Virginie Héloire"
			}

			entity {
				key "entity_customer"
				title "Customer"
				enterprise "hello"
				type EntityType.CONCEPT
				field {
					title "id"
					description "the identifier of the customer"
					type "UUID"
				}
			}

			capability {
				key "sm"
				title "Sales and Marketing"
				enterprise "hello"

				capability {
					title "Channel Management"
					enterprise "hello"

					capability {
						title "Channel Content Management"
						enterprise "hello"
					}
				}
			}


			platform {
				key "platform1"
				title "A sample platform"
				description "This is a sample platform"
			}

			technology {
				key "java"
				title "Java"
				type TechnologyType.LANGUAGE_FRAMEWORK
				recommendation TechnologyRecommendation.ADOPT
			}

			technology {
				key "scala"
				title "Scala"
				type TechnologyType.LANGUAGE_FRAMEWORK
				recommendation TechnologyRecommendation.ADOPT
			}

			technology {
				key "nodejs"
				title "NodeJS"
				type TechnologyType.LANGUAGE_FRAMEWORK
				recommendation TechnologyRecommendation.ADOPT
			}

			def system1 = system {
				key "system1"
				title "A system without a platform"

				realizes {
					target "cms"
					title "realizes"
				}

				costs {
					fiscalYear {
						year 2022
						capex {
							title "Application Hosting"
							description "Azure App Service"
							category "Hosting"
							unitOfMeasure "monthly subscription"
							numberOfUnits 12
							costPerUnit 454
						}
					}
					fiscalYear(2023) {
						opex {
							title "Subscription"
							description "A Monthly subscription"
							category "Subscription"
							unitOfMeasure "month"
							numberOfUnits 12
							costPerUnit 50
						}
					}
				}

				ConstraintAspectConfigurer budget
				ConstraintAspectConfigurer principles
				FunctionalRequirementAspectConfigurer manageContent
				FunctionalRequirementAspectConfigurer personalization
				QualityAttributeRequirementAspectConfigurer performance
				QualityAttributeRequirementAspectConfigurer availability

				requirements {
					budget = constraint {
						title "Budget"
						description "We should be able to afford it"
						weight RequirementWeight.MUST_HAVE
					}
					principles = constraint {
						title "Architecture Principles"
						description "We should at least follow rent before buy before build"
					}
					manageContent = functionalRequirement {
						key "req_system1_cms"
						title "It should manage content"
					}
					personalization = functionalRequirement {
						title "Personalization"
						description "Personalized content must be manageable through the CMS"
						weight RequirementWeight.MUST_HAVE
					}
					performance = qar {
						key "qar_system1_performance"
						title "Performance"
						sourceOfStimulus "a user"
						stimulus "accesses the website"
						environment "when there are 1 millions users already there"
						response "the website should load normally"
						responseMeasure "in less than 1 second"
					}
					availability = qar {
						title "Availability"
						response "it needs to be available"
					}
				}
				scores {
					forConstraint(budget) {
						score ScoreWeight.EXCEEDS_EXPECTATIONS
						description "it's cheap!"
					}
					forConstraint principles, ScoreWeight.MEETS_EXPECTATIONS

					forFunctionalRequirement(manageContent) {
						score ScoreWeight.DOES_NOT_MEET_EXPECTATIONS
					}

					forFunctionalRequirement personalization, ScoreWeight.ALMOST_MEETS_EXPECTATIONS, "They are doing something, but not enough"

					forQar {
						withKey "qar_system1_performance"
						score ScoreWeight.ALMOST_MEETS_EXPECTATIONS
					}

					forQar availability, ScoreWeight.EXCEEDS_EXPECTATIONS, "This thing never goes down"
				}
			}

			system {
				key "system2"
				title "A system with a platform"
				platform "platform1"
				description "This system has a platform and containers as well"

				container {
					key "container1"
					title "A container within system 2"
					type ContainerType.MICROSERVICE
					technology "java"
					technologies "nodejs", "scala"

					flow {
						target "system1"
						title "a flow from container 1 to system 1"
						technology "java"
						description "with a description"
					}

					architectureVerdict {
						recommendation ArchitectureVerdictType.INVEST
						description "we should have bought this a long time ago"
					}

					swot {
						strength "it is strong"
						weakness "but also weak"
						opportunity "which is an opportunity"
						threat "and a threat"
					}
				}

				documentation {
					url "https://innovenso.com"
					documentation "This is a wonderful system"
					functional "it does things"
					api "https://innovenso.com/api"
				}

				externalIds {
					sparx "123"
				}

				lifecycle {
					production {
						date LocalDate.of(2021,10,15)
						title "in production"
					}
					phaseOut {
						date LocalDate.of(2021,11,15)
						title "phasing out already"
					}
					decommissioned {
						date LocalDate.of(2022,1,15)
					}
				}
			}

			systemIntegration {
				source "system1"
				target "system2"
				key "system1_system2_integration"
				title "IL467"
				description "an integration"
				criticality Criticality.HIGH
				failureImpact "customer does not get data"
				resilience "circuit breaker"
			}

			decision {
				key "ar43"
				title "Headless CMS Selection"
				description "We need a new CMS"
				enterprise "hello"

				requirements {
					functionalRequirement {
						title "It has to manage content"
						key "ar43_req_cms"
					}
				}

				currentState {
					title "it sucks"
					description "no one knows what to use"
				}

				goal {
					title "it works"
				}

				consequence {
					title "deploy it"
					description "which takes some work at least"
				}

				assumption {
					title "it exists"
					description "surely we can find one"
				}

				option {
					key "ar43_strapi"
					title "Strapi"
					verdict DecisionOptionVerdict.UNDER_INVESTIGATION
					description "Open Source CMS"

					scores {
						forFunctionalRequirement {
							withKey "ar43_req_cms"
							score ScoreWeight.MEETS_EXPECTATIONS
						}
					}

					costs {
						fiscalYear {
							year 2022
							capex {
								title "Application Hosting"
								description "Azure App Service"
								category "Hosting"
								unitOfMeasure "monthly subscription"
								numberOfUnits 12
								costPerUnit 454
							}
						}
					}
				}
			}

			project {
				key "pi6"
				title "Product Increment 6"
				description "This will be a busy one"
				type "product increment"
				enterprise "hello"

				milestone {
					key "pi6-1"
					title "Sprint 1 of PI 6"
					description "a milestone baby"

					lifecycle {
						due {
							date LocalDate.of(2021,12,15)
							title "should be finished"
						}
					}

					creates {
						target "system1"
						title "creates"
						description "during this sprint, the system will be built"
					}
				}
			}

			relationships {
				virginie.flow {
					target system1
					title "Virginie uses System 1"
				}
			}
		}
	}
}
