package zones

import com.innovenso.townplan.dsl.Zone

class Zone2 extends Zone {
	Zone2(Zone1 zone1) {
		concepts {
			capability {
				title "Finance"
				enterprise zone1.innovenso
			}

			system {
				title "Another system"
				description "which is fancy"

				flow {
					target zone1.system123
					title "a flow relationship"
				}

				realizes zone1.buildingBlock
			}
		}
	}
}
