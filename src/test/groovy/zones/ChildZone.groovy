package zones

import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.dsl.concepts.capability.BusinessCapabilityConfigurer
import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer

class ChildZone extends Zone {
	BusinessCapabilityConfigurer managingEa

	ChildZone(ParentZone parentZone) {
		concepts {
			this.managingEa = capability {
				title "Managing Enterprise Architecture"
				enterprise parentZone.theEnterprise
			}

			zone new GrandChildZone(parentZone, this)
		}
	}
}
