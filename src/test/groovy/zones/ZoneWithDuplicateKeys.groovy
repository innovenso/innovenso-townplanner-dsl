package zones

import com.innovenso.townplan.api.value.enums.ActorType
import com.innovenso.townplan.dsl.Zone

class ZoneWithDuplicateKeys extends Zone {
	ZoneWithDuplicateKeys() {
		concepts {
			enterprise {
				key "hello"
				title "world"
				description "not bad"
			}

			buildingBlock {
				key "cms"
				title "Content Management System"
				description "Will be Strapi no doubt"
				enterprise "hello"
			}

			actor {
				key "jurgen"
				title "Jurgen Lust"
				type ActorType.INDIVIDUAL
				enterprise "hello"
			}

			actor {
				key "hello"
				type ActorType.INDIVIDUAL
				enterprise "hello"
				title "Virginie Héloire"
			}

			capability {
				key "sm"
				title "Sales and Marketing"
				enterprise "hello"
			}

			capability {
				key "ccm"
				title "Channel Content Management"
				enterprise "hello"
				parent "cm"
			}

			capability {
				key "sm"
				title "Channel Management"
				enterprise "hello"
				parent "sm"
			}

			platform {
				key "platform1"
				title "A sample platform"
				description "This is a sample platform"
			}
		}
	}
}
