package zones

import com.innovenso.townplan.dsl.Zone
import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer

class ParentZone extends Zone {
	EnterpriseConfigurer theEnterprise

	ParentZone() {
		concepts {
			this.theEnterprise = enterprise {
				title "Innovenso BV"
				description "Enterprise Architecture as Code"
			}

			zone new ChildZone(this)
		}
	}
}
