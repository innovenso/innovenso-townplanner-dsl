package zones

import com.innovenso.townplan.api.value.enums.ActorType
import com.innovenso.townplan.dsl.Zone

class ZoneWithMissingDependencies extends Zone {
	ZoneWithMissingDependencies() {
		concepts {
			enterprise {
				key "hello"
				title "world"
				description "not bad"
			}

			buildingBlock {
				key "cms"
				title "Content Management System"
				description "Will be Strapi no doubt"
				enterprise "world"
			}

			actor {
				key "jurgen"
				title "Jurgen Lust"
				type ActorType.INDIVIDUAL
				enterprise "hello"
			}

			actor {
				key "virginie"
				type ActorType.INDIVIDUAL
				enterprise "apple"
				title "Virginie Héloire"
			}

			capability {
				key "sm"
				title "Sales and Marketing"
				enterprise "hello"
			}

			capability {
				key "ccm"
				title "Channel Content Management"
				enterprise "hello"
				parent "cm"
			}

			capability {
				key "cm"
				title "Channel Management"
				enterprise "hello"
				parent "unknown"
			}

			platform {
				key "platform1"
				title "A sample platform"
				description "This is a sample platform"
			}
		}
	}
}
