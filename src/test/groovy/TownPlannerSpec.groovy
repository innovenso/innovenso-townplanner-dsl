import com.innovenso.townplan.api.TownPlan
import com.innovenso.townplan.api.value.Element
import com.innovenso.townplan.api.value.Relationship
import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.api.value.aspects.ArchitectureVerdictType
import com.innovenso.townplan.api.value.aspects.SWOTType
import com.innovenso.townplan.api.value.aspects.SecurityImpactLevel
import com.innovenso.townplan.api.value.business.BusinessActor
import com.innovenso.townplan.api.value.business.Enterprise
import com.innovenso.townplan.api.value.enums.ActorType
import com.innovenso.townplan.api.value.enums.Criticality
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock
import com.innovenso.townplan.api.value.it.DataEntity
import com.innovenso.townplan.api.value.it.ItContainer
import com.innovenso.townplan.api.value.it.ItPlatform
import com.innovenso.townplan.api.value.it.ItProject
import com.innovenso.townplan.api.value.it.ItSystem
import com.innovenso.townplan.api.value.it.ItSystemIntegration
import com.innovenso.townplan.api.value.it.Technology
import com.innovenso.townplan.api.value.it.decision.Decision
import com.innovenso.townplan.api.value.it.principle.Principle
import com.innovenso.townplan.api.value.strategy.BusinessCapability
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanner
import com.innovenso.townplan.dsl.concepts.capability.BusinessCapabilityConfigurer
import jakarta.validation.ValidationException
import spock.lang.Specification
import zones.CorrectZone
import zones.ParentZone
import zones.Zone1
import zones.Zone2
import zones.ZoneWithDuplicateKeys
import zones.ZoneWithInvalidConfigurers
import zones.ZoneWithMissingDependencies

class TownPlannerSpec extends Specification {
	def "enterprises are created using DSL"() {
		expect:
		getElementsOfTypeFromCorrectTownPlan(Enterprise).size() == 1
	}

	def "capabilities are created using DSL"() {
		when:
		def capabilities = getElementsOfTypeFromCorrectTownPlan(BusinessCapability)
		then:
		capabilities.size() == 3
		capabilities.each {
			println "${it.sortKey} ${it.title}"
		}
	}

	def "architecture building blocks are created using DSL"() {
		expect:
		getElementsOfTypeFromCorrectTownPlan(ArchitectureBuildingBlock).size() == 1
	}

	def "actors are created using DSL"() {
		expect:
		getElementsOfTypeFromCorrectTownPlan(BusinessActor).size() == 2
	}

	def "IT Platforms are created using DSL"() {
		expect:
		getElementsOfTypeFromCorrectTownPlan(ItPlatform).size() == 1
	}

	def "Technologies are created using DSL"() {
		expect:
		getElementsOfTypeFromCorrectTownPlan(Technology).size() == 3
	}

	def "IT Systems are created using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.size() == 2
		systems.find {it.key == "system1"}.parent.isEmpty()
		systems.find {it.key == 'system2'}.parent.get().key == 'platform1'
	}

	def "IT System Integrations are created using DSL"() {
		when:
		def integrations = getElementsOfTypeFromCorrectTownPlan(ItSystemIntegration)
		then:
		integrations.size() == 1
		integrations.first().criticality.get() == Criticality.HIGH
		println integrations.first()
	}

	def "IT Containers are created using DSL"() {
		when:
		def containers = getElementsOfTypeFromCorrectTownPlan(ItContainer)
		then:
		containers.size() == 1
		containers.first().system.key == 'system2'
		containers.first().technologies.size() == 3
	}

	def "Flow relationships are created using DSL"() {
		when:
		def flows = getRelationshipsFromCorrectTownPlan(RelationshipType.FLOW)
		then:
		flows.findAll {it.source.key == 'container1' && it.target.key == 'system1'}.with {
			it.size() == 1
			it.first().relationshipType == RelationshipType.FLOW
		}
	}

	def "Realization relationships are created using DSL"() {
		when:
		def realizations = getRelationshipsFromCorrectTownPlan(RelationshipType.REALIZATION)
		then:
		realizations.findAll {it.source.key == 'system1' && it.target.key == 'cms'}.with {
			it.size() == 1
			it.first().relationshipType == RelationshipType.REALIZATION
		}
	}

	def "Constraints are created using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system1'}.getConstraints().with {
			it.size() == 2
			it.each {println it}
		}
	}

	def "Functional Requirements are created using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system1'}.getFunctionalRequirements().with {
			it.size() == 2
			it.each {println it}
		}
	}

	def "Quality Attribute Requirements are created using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system1'}.getQualityAttributes().with {
			it.size() == 2
			it.each {println it}
		}
	}

	def "Constraint scores are created using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system1'}.getConstraintScores().with {
			it.size() == 2
			it.each {println it}
		}
	}

	def "Functional Requirement scores are created using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system1'}.getFunctionalRequirementScores().with {
			it.size() == 2
			it.each {println it}
		}
	}

	def "Quality Attribute Requirement scores are created using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system1'}.getQualityAttributeRequirementScores().with {
			it.size() == 2
			it.each {println it}
		}
	}

	def "Costs are created using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system1'}.getCosts().with {
			it.size() == 2
			it.each {println it}
		}
	}

	def "Entities are created using DSL"() {
		when:
		def entities = getElementsOfTypeFromCorrectTownPlan(DataEntity)
		then:
		entities.find{it.key == 'entity_customer'}.with {
			it.title == 'Customer'
			it.fields.size() == 1
			it.fields.each {println it}
		}
	}

	def "Decisions are created using DSL"() {
		when:
		def entities = getElementsOfTypeFromCorrectTownPlan(Decision)
		then:
		entities.find{it.key == "ar43"}.with {
			it.title == "Headless CMS Selection"
			it.contexts.size() == 4
			it.options.first().with { option ->
				option.costs.size() == 1
				option.functionalRequirementScores.size() == 1
				option.title == "Strapi"
			}
		}
	}

	def "Documentation is added using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system2'}.getDocumentations().with {
			it.size() == 4
			it.each {println it}
		}
	}

	def "External ID is added using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		then:
		systems.find {it.key == 'system2'}.getExternalIds().with {
			it.size() == 1
			it.each {println it}
		}
	}

	def "Time Machine is added using DSL"() {
		when:
		def townPlan = TownPlanner.townPlan {
			zone new CorrectZone()
		}
		def pointsInTime = townPlan.getKeyPointsInTime()
		then:
		pointsInTime.size() == 3
		pointsInTime.each {println it}
	}

	def "Lifecycle is added using DSL"() {
		when:
		def systems = getElementsOfTypeFromCorrectTownPlan(ItSystem)
		def fatherTimes = systems.find {it.key == 'system2'}.getFatherTimes()
		then:
		fatherTimes.size() == 3
		fatherTimes.each {println it}
	}

	def "Projects and milestones are added using DSL"() {
		when:
		def projects = getElementsOfTypeFromCorrectTownPlan(ItProject)
		def milestones = projects.find {it.key == 'pi6'}.getMilestones()
		then:
		projects.size() == 1
		projects.first().type == 'product increment'
		milestones.size() == 1
		milestones.first().getFatherTimes().size() == 1
		milestones.first().getFatherTimes().first().title == 'should be finished'
	}

	def "Architecture Verdict is set using DSL"() {
		when:
		def containers = getElementsOfTypeFromCorrectTownPlan(ItContainer)
		def container = containers.find{it.key == 'container1'}
		then:
		container.getArchitectureVerdict().verdictType == ArchitectureVerdictType.INVEST
	}

	def "SWOT is assigned using DSL"() {
		when:
		def containers = getElementsOfTypeFromCorrectTownPlan(ItContainer)
		def container = containers.find{it.key == 'container1'}
		then:
		container.getSwots().size() == 4
		SWOTType.values().each {
			container.getSwots(it).size() == 1
		}
	}

	def "Principles are added using DSL"() {
		when:
		def townplan = TownPlanner.townPlan {
			zone {
				enterprise {
					key "hello"
					title "world"
				}

				principle {
					key "principle_chaos"
					title "Chaos"
					description "Chaos happens, design for it"
					type "design principle"
					enterprise "hello"
				}
			}
		}
		def principles = townplan.getElements(Principle)
		then:
		principles.size() == 1
		principles.first().title == "Chaos"
	}

	def "security aspects are added using DSL"() {
		when:
		def townplan = TownPlanner.townPlan {
			zone {
				enterprise {
					key "hello"
					title "world"
				}

				project {
					key "pi6"
					title "Product Increment 6"
					description "This will be a busy one"
					type "product increment"
					enterprise "hello"

					security {
						confidentiality {
							level SecurityImpactLevel.MEDIUM
							description "it can have an impact"
						}

						gdpr {
							description "No personal data are stored anywhere"
						}
					}
				}
			}
		}
		def project = townplan.getElement(ItProject, 'pi6').get()
		then:
		project.getSecurityConcerns().size() == 1
		project.getSecurityImpacts().size() == 1
	}

	def "referencing elements between zones is possible"() {
		when:
		def zone1 = new Zone1()
		def zone2 = new Zone2(zone1)
		def townPlan = TownPlanner.townPlan {
			zone zone1
			zone zone2
		}
		then:
		townPlan.getElements(BusinessCapability).size() == 11
		townPlan.getElements(ArchitectureBuildingBlock).size() == 1
		townPlan.getElements(Enterprise).size() == 1
		townPlan.getElements(ItSystem).size() == 2
		townPlan.getAllRelationships().size() == 5

		townPlan.getElements(BusinessCapability).each {
			println "${it.sortKey} ${it.title}"
		}
	}

	def "relationships can be added in a dedicated relationships block"() {
		when:
		def townPlan = TownPlanner.townPlan {
			zone {
				def innovenso = enterprise {
					title = "Innovenso BV"
				}

				def system1 = system {
					title "System 1"
				}

				def virginie = actor {
					title "Virginie Héloire"
					type ActorType.INDIVIDUAL
					enterprise innovenso
				}

				relationships {
					virginie.uses system1, "does something amazing"
				}
			}
		}
		then:
		townPlan.getAllRelationships().size() == 1
		townPlan.getAllRelationships().each {println it}
	}

	def "flow views are added using DSL"() {
		when:
		def townPlan = TownPlanner.townPlan {
			zone {
				def innovenso = enterprise {
					title = "Innovenso BV"
				}

				def system1 = system {
					title "System 1"
				}

				def system2 = system {
					title "System 2"
				}

				def virginie = actor {
					title "Virginie Héloire"
					type ActorType.INDIVIDUAL
					enterprise innovenso
				}

				relationships {
					virginie.uses system1, "does something amazing"
					system1.uses system2, "delegates something amazing"
				}

				flowView {
					title "A Test Flow View"
					description "To verify whether flow views are created correctly using DSL"
					showStepCounter true

					request {
						from virginie
						to system1
						title 'requests something amazing'
					}

					request {
						from system1
						to system2
						title 'I delegate this'
					}

					response {
						from system2
						to system1
						title 'I did it'
					}

					response {
						from system1
						to virginie
						title 'He did it'
					}

					message {
						from virginie
						to system1
						title 'Thanks'
					}
				}
			}
		}
		then:
		townPlan.getFlowViews().size() == 1
		townPlan.getFlowViews().first().title == 'A Test Flow View'
		townPlan.getFlowViews().first().steps.size() == 5
		townPlan.getFlowViews().first().with {
			println it.title
			println it.description
			println it.isShowStepCounter()
			steps.each {
				println "${it.relationship.key}: ${it.title} (response ${it.response})"
			}
		}
	}

	def "Zones can have subzones"() {
		when:
		def townPlan = TownPlanner.townPlan {
			zone new ParentZone()
		}
		then:
		townPlan.getElements(Enterprise.class).size() == 1
		townPlan.getElements(BusinessCapability.class).size() == 1
		townPlan.getElements(ArchitectureBuildingBlock.class).size() == 1
	}

	def "invalid or missing values in a concept configurer result in a ValidationException"() {
		when:
		TownPlanner.townPlan {
			zone new ZoneWithInvalidConfigurers()
		}
		then:
		thrown ValidationException
	}

	def "duplicate keys result in a ValidationException"() {
		when:
		TownPlanner.townPlan {
			zone new ZoneWithDuplicateKeys()
		}
		then:
		thrown ValidationException
	}

	def "missing dependencies result in a ValidationException"() {
		when:
		TownPlanner.townPlan {
			zone new ZoneWithMissingDependencies()
		}
		then:
		thrown ValidationException
	}

	def "default key generation works"() {
		when:
		def townPlan = TownPlanner.townPlan {
			zone {
				def innovenso = enterprise {
					title "Innovenso BV"
				}
				capability {
					title "Sales & Marketing"
					enterprise innovenso
				}
				system {
					title "a system"

					costs {
						fiscalYear(2022) {
							opex {
								title "Hosting"
								category"Hosting"
								unitOfMeasure "month"
								numberOfUnits 12
								costPerUnit 100
							}
						}
					}
				}
			}
		}
		then:
		townPlan.getElements(ItSystem).first().key == 'it_system_a_system'
		townPlan.getElements(BusinessCapability).first().key == 'business_capability_sales___marketing'
		townPlan.getElements(Enterprise).first().key == 'enterprise_innovenso_bv'
		townPlan.getElements(ItSystem).first().costs.first().key == 'it_system_a_system_cost_entry_hosting'
	}

	private static <T extends Element> Set<T> getElementsOfTypeFromCorrectTownPlan(Class<T> elementClass) {
		TownPlanImpl townPlan = TownPlanner.townPlan {
			zone new CorrectZone()
		}
		Set<T> elements = townPlan.getElements(elementClass)
		elements.each { println it}
		return elements
	}

	private static Set<Relationship> getRelationshipsFromCorrectTownPlan(RelationshipType relationshipType) {
		TownPlanImpl townPlan = TownPlanner.townPlan {
			zone new CorrectZone()
		}
		Set<Relationship> relationships = townPlan.getAllRelationships().findAll {it.relationshipType == relationshipType}
		relationships.each { println it}
		return relationships
	}
}
