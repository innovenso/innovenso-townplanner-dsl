package com.innovenso.townplan.dsl

import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.traits.CanConfigureConfigurer
import jakarta.validation.Validation
import jakarta.validation.Validator
import jakarta.validation.ValidatorFactory
import lombok.NonNull

abstract class AbstractConfigurer implements CanConfigureConfigurer {
	private final TownPlanConfigurer townPlanConfigurer

	AbstractConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		this.townPlanConfigurer = townPlanConfigurer
	}

	Collection<String> validate() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
		Validator validator = factory.getValidator()
		return validator.validate(this).collect { "${configurerDescription()}: ${it.propertyPath.toString()} - ${it.message}" as String }
	}

	public TownPlanConfigurer getTownPlanConfigurer() {
		return townPlanConfigurer
	}

	abstract def apply(@NonNull TownPlanImpl townPlan)

	abstract String configurerDescription()

	@Override
	public String toString() {
		return configurerDescription()
	}

	List<String> dependsOnKeys() {
		[]
	}
}
