package com.innovenso.townplan.dsl

import lombok.NonNull

class SortKeyContext {
	private final Map<String,Integer> counterMap = [:]
	private String prefix = ""

	public String getPrefix() {
		return prefix
	}

	public void setPrefix(String prefixToSet) {
		if (prefixToSet) this.prefix = "${prefixToSet}."
		else this.prefix = ""
	}

	String nextSortKey(@NonNull String sortableType) {
		Integer currentCount = ++counterMap.getOrDefault(sortableType, 0)
		counterMap.put(sortableType, currentCount)
		"${prefix}${currentCount}"
	}
}
