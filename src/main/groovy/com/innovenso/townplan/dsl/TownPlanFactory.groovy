package com.innovenso.townplan.dsl

import com.innovenso.townplan.api.TownPlan

interface TownPlanFactory {
	TownPlan getTownPlan()
}