package com.innovenso.townplan.dsl

import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.concepts.actor.ActorConfigurer
import com.innovenso.townplan.dsl.concepts.buildingblock.ArchitectureBuildingBlockConfigurer
import com.innovenso.townplan.dsl.concepts.capability.BusinessCapabilityConfigurer
import com.innovenso.townplan.dsl.concepts.decision.DecisionConfigurer
import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer
import com.innovenso.townplan.dsl.concepts.entity.DataEntityConfigurer
import com.innovenso.townplan.dsl.concepts.integration.ITSystemIntegrationConfigurer
import com.innovenso.townplan.dsl.concepts.platform.PlatformConfigurer
import com.innovenso.townplan.dsl.concepts.principle.PrincipleConfigurer
import com.innovenso.townplan.dsl.concepts.project.ProjectConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipGroupConfigurer
import com.innovenso.townplan.dsl.concepts.system.ITSystemConfigurer
import com.innovenso.townplan.dsl.concepts.technology.TechnologyConfigurer
import com.innovenso.townplan.dsl.timemachine.TimeMachineConfigurer
import com.innovenso.townplan.dsl.traits.HasSortKeyContext
import com.innovenso.townplan.dsl.views.FlowViewConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_FIRST
import static groovy.lang.Closure.DELEGATE_ONLY

class ZoneConfigurer extends AbstractConfigurer implements HasSortKeyContext {

	ZoneConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	@Override
	SortKeyContext getSortKeyContext() {
		return townPlanConfigurer.getSortKeyContext()
	}

	ZoneConfigurer zone(@DelegatesTo(value = ZoneConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.zone configureConfigurer(new ZoneConfigurer(townPlanConfigurer), configurer)
	}

	ZoneConfigurer zone(Zone zone) {
		townPlanConfigurer.zone zone
	}

	TimeMachineConfigurer timeMachine(@DelegatesTo(value = TimeMachineConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new TimeMachineConfigurer(townPlanConfigurer), configurer)
	}

	EnterpriseConfigurer enterprise(@DelegatesTo(value = EnterpriseConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.enterprise configureConfigurer(new EnterpriseConfigurer(townPlanConfigurer), configurer)
	}

	ActorConfigurer actor(@DelegatesTo(value = ActorConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.actor configureConfigurer(new ActorConfigurer(townPlanConfigurer), configurer)
	}

	ArchitectureBuildingBlockConfigurer buildingBlock(@DelegatesTo(value = ArchitectureBuildingBlockConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.architectureBuildingBlock configureConfigurer(new ArchitectureBuildingBlockConfigurer(townPlanConfigurer), configurer)
	}

	BusinessCapabilityConfigurer capability(@DelegatesTo(value = BusinessCapabilityConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.businessCapability configureConfigurer(new BusinessCapabilityConfigurer(townPlanConfigurer, getSortKeyContext().nextSortKey("Business Capability")), configurer)
	}

	PlatformConfigurer platform(@DelegatesTo(value = PlatformConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.platform configureConfigurer(new PlatformConfigurer(townPlanConfigurer), configurer)
	}

	ITSystemConfigurer system(@DelegatesTo(value = ITSystemConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.system configureConfigurer(new ITSystemConfigurer(townPlanConfigurer), configurer)
	}

	TechnologyConfigurer technology(@DelegatesTo(value = TechnologyConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.technology configureConfigurer(new TechnologyConfigurer(townPlanConfigurer), configurer)
	}

	DataEntityConfigurer entity(@DelegatesTo(value = DataEntityConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.entity configureConfigurer(new DataEntityConfigurer(townPlanConfigurer), configurer)
	}

	DecisionConfigurer decision(@DelegatesTo(value = DecisionConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.decision configureConfigurer(new DecisionConfigurer(townPlanConfigurer), configurer)
	}

	ProjectConfigurer project(@DelegatesTo(value = ProjectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.project configureConfigurer(new ProjectConfigurer(townPlanConfigurer), configurer)
	}

	PrincipleConfigurer principle(@DelegatesTo(value = PrincipleConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.principle configureConfigurer(new PrincipleConfigurer(townPlanConfigurer, nextSortKey("Principle")), configurer)
	}

	ITSystemIntegrationConfigurer systemIntegration(@DelegatesTo(value = ITSystemIntegrationConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.integration configureConfigurer(new ITSystemIntegrationConfigurer(townPlanConfigurer), configurer)
	}

	RelationshipGroupConfigurer relationships(@DelegatesTo(value=RelationshipGroupConfigurer, strategy = DELEGATE_FIRST) Closure configurer) {
		configureConfigurer(new RelationshipGroupConfigurer(townPlanConfigurer), configurer)
	}

	FlowViewConfigurer flowView(@DelegatesTo(value = FlowViewConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.flowView configureConfigurer(new FlowViewConfigurer(townPlanConfigurer), configurer)
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		return null
	}

	@Override
	String configurerDescription() {
		return "Zone"
	}
}
