package com.innovenso.townplan.dsl

import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_FIRST

class Zone {
	Closure configurer

	def concepts(@DelegatesTo(value = ZoneConfigurer, strategy = DELEGATE_FIRST) Closure configurer) {
		this.configurer = configurer
	}

	ZoneConfigurer apply(TownPlanConfigurer townPlanConfigurer) {
		if (configurer == null) return
			def zoneConfigurer = new ZoneConfigurer(townPlanConfigurer)
		configurer.delegate = zoneConfigurer
		configurer.resolveStrategy = DELEGATE_FIRST
		configurer.run()
		zoneConfigurer
	}
}
