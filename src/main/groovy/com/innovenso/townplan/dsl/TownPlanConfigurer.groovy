package com.innovenso.townplan.dsl

import com.innovenso.eventsourcing.api.id.EntityId
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.aspects.cost.CostEntryConfigurer
import com.innovenso.townplan.dsl.aspects.documentation.DocumentationAspectConfigurer
import com.innovenso.townplan.dsl.aspects.external.ExternalIdAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.ConstraintAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.FunctionalRequirementAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.QualityAttributeRequirementAspectConfigurer
import com.innovenso.townplan.dsl.aspects.scores.ConstraintScoreAspectConfigurer
import com.innovenso.townplan.dsl.aspects.scores.FunctionalRequirementScoreAspectConfigurer
import com.innovenso.townplan.dsl.aspects.scores.QualityAttributeRequirementScoreAspectConfigurer
import com.innovenso.townplan.dsl.aspects.security.SecurityConcernAspectConfigurer
import com.innovenso.townplan.dsl.aspects.security.SecurityImpactAspectConfigurer
import com.innovenso.townplan.dsl.aspects.swot.SwotAspectConfigurer
import com.innovenso.townplan.dsl.aspects.timemachine.FatherTimeConfigurer
import com.innovenso.townplan.dsl.aspects.verdict.ArchitectureVerdictAspectConfigurer
import com.innovenso.townplan.dsl.concepts.actor.ActorConfigurer
import com.innovenso.townplan.dsl.concepts.buildingblock.ArchitectureBuildingBlockConfigurer
import com.innovenso.townplan.dsl.concepts.capability.BusinessCapabilityConfigurer
import com.innovenso.townplan.dsl.concepts.container.ITContainerConfigurer
import com.innovenso.townplan.dsl.concepts.decision.DecisionConfigurer
import com.innovenso.townplan.dsl.concepts.decision.DecisionContextConfigurer
import com.innovenso.townplan.dsl.concepts.decision.DecisionOptionConfigurer
import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer
import com.innovenso.townplan.dsl.concepts.entity.DataEntityConfigurer
import com.innovenso.townplan.dsl.concepts.entity.DataEntityFieldConfigurer
import com.innovenso.townplan.dsl.concepts.integration.ITSystemIntegrationConfigurer
import com.innovenso.townplan.dsl.concepts.platform.PlatformConfigurer
import com.innovenso.townplan.dsl.concepts.principle.PrincipleConfigurer
import com.innovenso.townplan.dsl.concepts.project.ProjectConfigurer
import com.innovenso.townplan.dsl.concepts.project.ProjectMilestoneConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import com.innovenso.townplan.dsl.concepts.system.ITSystemConfigurer
import com.innovenso.townplan.dsl.concepts.technology.TechnologyConfigurer
import com.innovenso.townplan.dsl.timemachine.KeyPointInTimeConfigurer
import com.innovenso.townplan.dsl.traits.HasSortKeyContext
import com.innovenso.townplan.dsl.views.FlowViewConfigurer
import com.innovenso.townplan.dsl.views.FlowViewStepConfigurer
import jakarta.validation.ValidationException
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_FIRST

class TownPlanConfigurer implements HasSortKeyContext {
	String key = "townplan"
	List<EnterpriseConfigurer> enterpriseConfigurers = []
	List<ActorConfigurer> actorConfigurers = []
	List<ArchitectureBuildingBlockConfigurer> architectureBuildingBlockConfigurers = []
	List<BusinessCapabilityConfigurer> businessCapabilityConfigurers = []
	List<PlatformConfigurer> platformConfigurers = []
	List<ITSystemConfigurer> systemConfigurers = []
	List<TechnologyConfigurer> technologyConfigurers = []
	List<ITContainerConfigurer> containerConfigurers = []
	List<RelationshipConfigurer> relationshipConfigurers = []
	List<ConstraintAspectConfigurer> constraintAspectConfigurers = []
	List<FunctionalRequirementAspectConfigurer> functionalRequirementAspectConfigurers = []
	List<QualityAttributeRequirementAspectConfigurer> qarConfigurers = []
	List<ConstraintScoreAspectConfigurer> constraintScoreAspectConfigurers = []
	List<FunctionalRequirementScoreAspectConfigurer> functionalRequirementScoreAspectConfigurers = []
	List<QualityAttributeRequirementScoreAspectConfigurer> qualityAttributeRequirementScoreAspectConfigurers = []
	List<CostEntryConfigurer> costEntryConfigurers = []
	List<DataEntityConfigurer> dataEntityConfigurers = []
	List<DataEntityFieldConfigurer> dataEntityFieldConfigurers = []
	List<DecisionConfigurer> decisionConfigurers = []
	List<DecisionContextConfigurer> decisionContextConfigurers = []
	List<DecisionOptionConfigurer> decisionOptionConfigurers = []
	List<DocumentationAspectConfigurer> documentationAspectConfigurers = []
	List<ExternalIdAspectConfigurer> externalIdAspectConfigurers = []
	List<KeyPointInTimeConfigurer> pointInTimeConfigurers = []
	List<FatherTimeConfigurer> fatherTimeConfigurers = []
	List<ProjectConfigurer> projectConfigurers = []
	List<ProjectMilestoneConfigurer> projectMilestoneConfigurers = []
	List<ArchitectureVerdictAspectConfigurer> architectureVerdictAspectConfigurers = []
	List<SwotAspectConfigurer> swotAspectConfigurers = []
	List<PrincipleConfigurer> principleConfigurers = []
	List<ITSystemIntegrationConfigurer> integrationConfigurers = []
	List<SecurityConcernAspectConfigurer> securityConcernAspectConfigurers = []
	List<SecurityImpactAspectConfigurer> securityImpactAspectConfigurers = []
	List<FlowViewConfigurer> flowViewConfigurers = []
	List<FlowViewStepConfigurer> flowViewStepConfigurers = []

	private List<AbstractConfigurer> allConfigurersSorted() {
		pointInTimeConfigurers + enterpriseConfigurers + principleConfigurers + sortedBusinessCapabilityConfigurers() + architectureBuildingBlockConfigurers + actorConfigurers + platformConfigurers + technologyConfigurers + systemConfigurers + containerConfigurers + integrationConfigurers + dataEntityConfigurers + dataEntityFieldConfigurers + decisionConfigurers + decisionContextConfigurers + decisionOptionConfigurers + projectConfigurers + projectMilestoneConfigurers + relationshipConfigurers + constraintAspectConfigurers + functionalRequirementAspectConfigurers + qarConfigurers + constraintScoreAspectConfigurers + functionalRequirementScoreAspectConfigurers + qualityAttributeRequirementScoreAspectConfigurers + costEntryConfigurers + documentationAspectConfigurers + externalIdAspectConfigurers + fatherTimeConfigurers + architectureVerdictAspectConfigurers + swotAspectConfigurers + securityConcernAspectConfigurers + securityImpactAspectConfigurers + flowViewConfigurers + flowViewStepConfigurers as List<AbstractConfigurer>
	}

	private List<AbstractConfigurer> allConfigurers() {
		pointInTimeConfigurers + enterpriseConfigurers + principleConfigurers + businessCapabilityConfigurers + architectureBuildingBlockConfigurers + actorConfigurers + platformConfigurers + technologyConfigurers + systemConfigurers + containerConfigurers + integrationConfigurers + dataEntityConfigurers + dataEntityFieldConfigurers + decisionConfigurers + decisionContextConfigurers + decisionOptionConfigurers + projectConfigurers + projectMilestoneConfigurers + relationshipConfigurers + constraintAspectConfigurers + functionalRequirementAspectConfigurers + qarConfigurers + constraintScoreAspectConfigurers + functionalRequirementScoreAspectConfigurers + qualityAttributeRequirementScoreAspectConfigurers + costEntryConfigurers + documentationAspectConfigurers + externalIdAspectConfigurers + fatherTimeConfigurers + architectureVerdictAspectConfigurers + swotAspectConfigurers + securityConcernAspectConfigurers + securityImpactAspectConfigurers + flowViewConfigurers + flowViewStepConfigurers as List<AbstractConfigurer>
	}

	TownPlanImpl apply() {
		TownPlanImpl townPlan = new TownPlanImpl(new EntityId(key))
		validateConfigurers(townPlan)
		validateDuplicates(townPlan)
		applyConfigurers(townPlan)
		return townPlan
	}

	private void validateConfigurers(@NonNull TownPlanImpl townPlan) {
		Set<String> validationMessages = new HashSet<>()
		allConfigurers().each { validationMessages.addAll(it.validate()) }
		if (validationMessages) {
			validationMessages.each { println it }
			throw new ValidationException("Town Plan validation failed")
		}
	}

	private void validateDuplicates(@NonNull TownPlanImpl townPlan) {
		Set<String> keys = []
		Set<String> duplicateKeys = []
		allConfigurers().each {
			if (keys.contains(it.key)) duplicateKeys.add(it.key)
			keys.add(it.key)
		}
		if (duplicateKeys) {
			duplicateKeys.each { println "found duplicate key: " + it }
			throw new ValidationException("Town Plan has duplicate keys")
		}
	}

	private void applyConfigurers(@NonNull TownPlanImpl townPlan) {
		allConfigurersSorted().each {
			validateDependencies(it, townPlan)
			it.apply(townPlan)
		}
	}

	private static void validateDependencies(AbstractConfigurer configurer, TownPlanImpl townPlan) {
		List<String> missingDependencies = configurer.dependsOnKeys().findAll { it != null }.findAll { townPlan.getModelComponent(it).isEmpty() }
		if (missingDependencies) {
			missingDependencies.each { println "missing dependency in ${configurer}: ${it}" }
			throw new ValidationException("Town Plan has missing dependencies")
		}
	}

	EnterpriseConfigurer enterprise(EnterpriseConfigurer configurer) {
		enterpriseConfigurers.add(configurer)
		configurer
	}

	String key(@NonNull String key) {
		this.key = key
		key
	}

	ActorConfigurer actor(ActorConfigurer configurer) {
		actorConfigurers.add(configurer)
		configurer
	}

	ArchitectureBuildingBlockConfigurer architectureBuildingBlock(ArchitectureBuildingBlockConfigurer configurer) {
		architectureBuildingBlockConfigurers.add(configurer)
		configurer
	}

	BusinessCapabilityConfigurer businessCapability(BusinessCapabilityConfigurer configurer) {
		businessCapabilityConfigurers.add(configurer)
		configurer
	}

	PlatformConfigurer platform(PlatformConfigurer configurer) {
		platformConfigurers.add(configurer)
		configurer
	}

	ITSystemConfigurer system(ITSystemConfigurer configurer) {
		systemConfigurers.add(configurer)
		configurer
	}

	TechnologyConfigurer technology(TechnologyConfigurer configurer) {
		technologyConfigurers.add(configurer)
		configurer
	}

	ITContainerConfigurer container(ITContainerConfigurer configurer) {
		containerConfigurers.add(configurer)
		configurer
	}

	RelationshipConfigurer relationship(RelationshipConfigurer configurer) {
		relationshipConfigurers.add(configurer)
		configurer
	}

	ConstraintAspectConfigurer constraint(ConstraintAspectConfigurer configurer) {
		constraintAspectConfigurers.add(configurer)
		configurer
	}

	FunctionalRequirementAspectConfigurer functionalRequirement(FunctionalRequirementAspectConfigurer configurer) {
		functionalRequirementAspectConfigurers.add(configurer)
		configurer
	}

	QualityAttributeRequirementAspectConfigurer qar(QualityAttributeRequirementAspectConfigurer configurer) {
		qarConfigurers.add(configurer)
		configurer
	}

	ConstraintScoreAspectConfigurer constraintScore(ConstraintScoreAspectConfigurer configurer) {
		this.constraintScoreAspectConfigurers.add(configurer)
		configurer
	}

	FunctionalRequirementScoreAspectConfigurer functionalRequirementScore(FunctionalRequirementScoreAspectConfigurer configurer) {
		this.functionalRequirementScoreAspectConfigurers.add(configurer)
		configurer
	}

	QualityAttributeRequirementScoreAspectConfigurer qarScore(QualityAttributeRequirementScoreAspectConfigurer configurer) {
		this.qualityAttributeRequirementScoreAspectConfigurers.add(configurer)
		configurer
	}

	CostEntryConfigurer cost(CostEntryConfigurer configurer) {
		this.costEntryConfigurers.add(configurer)
		configurer
	}

	DataEntityConfigurer entity(DataEntityConfigurer configurer) {
		this.dataEntityConfigurers.add(configurer)
		configurer
	}

	DataEntityFieldConfigurer entityField(DataEntityFieldConfigurer configurer) {
		this.dataEntityFieldConfigurers.add(configurer)
		configurer
	}

	DecisionConfigurer decision(DecisionConfigurer configurer) {
		this.decisionConfigurers.add(configurer)
		configurer
	}

	DecisionContextConfigurer decisionContext(DecisionContextConfigurer configurer) {
		this.decisionContextConfigurers.add(configurer)
		configurer
	}

	DecisionOptionConfigurer decisionOption(DecisionOptionConfigurer configurer) {
		this.decisionOptionConfigurers.add(configurer)
		configurer
	}

	DocumentationAspectConfigurer documentation(DocumentationAspectConfigurer configurer) {
		this.documentationAspectConfigurers.add(configurer)
		configurer
	}

	ExternalIdAspectConfigurer externalId(ExternalIdAspectConfigurer configurer) {
		this.externalIdAspectConfigurers.add(configurer)
		configurer
	}

	KeyPointInTimeConfigurer pointInTime(KeyPointInTimeConfigurer configurer) {
		this.pointInTimeConfigurers.add(configurer)
		configurer
	}

	FatherTimeConfigurer fatherTime(FatherTimeConfigurer configurer) {
		this.fatherTimeConfigurers.add(configurer)
		configurer
	}

	ProjectConfigurer project(ProjectConfigurer configurer) {
		this.projectConfigurers.add(configurer)
		configurer
	}

	ProjectMilestoneConfigurer projectMilestone(ProjectMilestoneConfigurer configurer) {
		this.projectMilestoneConfigurers.add(configurer)
		configurer
	}

	ArchitectureVerdictAspectConfigurer architectureVerdict(ArchitectureVerdictAspectConfigurer configurer) {
		this.architectureVerdictAspectConfigurers.add(configurer)
		configurer
	}

	SwotAspectConfigurer swot(SwotAspectConfigurer configurer) {
		this.swotAspectConfigurers.add(configurer)
		configurer
	}

	PrincipleConfigurer principle(PrincipleConfigurer configurer) {
		this.principleConfigurers.add(configurer)
		configurer
	}

	ITSystemIntegrationConfigurer integration(ITSystemIntegrationConfigurer configurer) {
		this.integrationConfigurers.add(configurer)
		configurer
	}

	SecurityConcernAspectConfigurer securityConcern(SecurityConcernAspectConfigurer configurer) {
		this.securityConcernAspectConfigurers.add(configurer)
		configurer
	}

	SecurityImpactAspectConfigurer securityImpact(SecurityImpactAspectConfigurer configurer) {
		this.securityImpactAspectConfigurers.add(configurer)
		configurer
	}

	FlowViewConfigurer flowView(FlowViewConfigurer configurer) {
		this.flowViewConfigurers.add(configurer)
		configurer
	}

	FlowViewStepConfigurer flowViewStep(FlowViewStepConfigurer configurer) {
		this.flowViewStepConfigurers.add(configurer)
		configurer
	}

	ZoneConfigurer zone(Zone zoneConfiguration) {
		ZoneConfigurer zoneConfigurer = zoneConfiguration.apply(this)
		zoneConfigurer
	}

	ZoneConfigurer zone(ZoneConfigurer configurer) {
		configurer()
		configurer
	}

	ZoneConfigurer zone(@DelegatesTo(value = ZoneConfigurer, strategy = DELEGATE_FIRST) Closure configurer) {
		ZoneConfigurer zoneConfigurer = new ZoneConfigurer(this)
		configurer.delegate = zoneConfigurer
		configurer.resolveStrategy = DELEGATE_FIRST
		configurer()
		zoneConfigurer
	}

	private List<BusinessCapabilityConfigurer> sortedBusinessCapabilityConfigurers() {
		List<BusinessCapabilityConfigurer> sortedList = new ArrayList<>()
		addBusinessCapabilityConfigurers(null, sortedList)
		return sortedList
	}

	private void addBusinessCapabilityConfigurers(String parent, List<BusinessCapabilityConfigurer> sortedList) {
		businessCapabilityConfigurers.findAll { it.parent == parent }.each {
			sortedList.add(it)
			addBusinessCapabilityConfigurers(it.key, sortedList)
		}
	}
}
