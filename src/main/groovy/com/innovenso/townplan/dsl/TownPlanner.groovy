package com.innovenso.townplan.dsl

import com.innovenso.townplan.domain.TownPlanImpl

import static groovy.lang.Closure.DELEGATE_FIRST

class TownPlanner {
	static TownPlanImpl townPlan(
			@DelegatesTo(value = TownPlanConfigurer, strategy = DELEGATE_FIRST) Closure configurer) {
		def townPlanConfigurer = new TownPlanConfigurer()
		configurer.delegate = townPlanConfigurer
		configurer.resolveStrategy = DELEGATE_FIRST
		configurer()
		return townPlanConfigurer.apply()
	}
}
