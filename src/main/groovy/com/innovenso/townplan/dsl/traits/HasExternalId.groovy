package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.external.ExternalIdAspectConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasExternalId extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void externalIds(@DelegatesTo(value = ExternalIdAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().externalId configureConfigurer(new ExternalIdAspectConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}