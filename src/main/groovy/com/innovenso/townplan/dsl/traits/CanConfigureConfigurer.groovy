package com.innovenso.townplan.dsl.traits


import static groovy.lang.Closure.DELEGATE_ONLY

trait CanConfigureConfigurer {

	public <T> T configureConfigurer(T actualConfigurer, Closure configurerClosure) {
		configurerClosure.delegate = actualConfigurer
		configurerClosure.resolveStrategy = DELEGATE_ONLY
		configurerClosure.call()
		return actualConfigurer
	}
}