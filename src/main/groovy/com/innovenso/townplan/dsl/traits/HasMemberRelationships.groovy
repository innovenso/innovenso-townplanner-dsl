package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasMemberRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	RelationshipConfigurer isMemberOf(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.MEMBER, getKey()), configurer)
	}

	RelationshipConfigurer isMemberOf(@NonNull String targetKey) {
		isMemberOf(targetKey, "is member of")
	}

	RelationshipConfigurer isMemberOf(@NonNull String targetKey, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.MEMBER, getKey(), targetKey, title)
	}

	RelationshipConfigurer isMemberOf(@NonNull AbstractConceptConfigurer target) {
		isMemberOf(target, "is member of")
	}

	RelationshipConfigurer isMemberOf(@NonNull AbstractConceptConfigurer target, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.MEMBER, getKey(), target.key, title)
	}
}