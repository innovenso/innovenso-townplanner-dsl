package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasErdRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void hasOneOrMore(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ONE_OR_MORE, getKey()), configurer)
	}

	RelationshipConfigurer hasOneOrMore(@NonNull String targetKey) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ONE_OR_MORE, getKey(), targetKey, "has one or more")
	}

	RelationshipConfigurer hasOneOrMore(@NonNull AbstractConceptConfigurer target) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ONE_OR_MORE, getKey(), target.key, "has one or more")
	}

	void hasZeroOrMore(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ZERO_OR_MORE, getKey()), configurer)
	}

	RelationshipConfigurer hasZeroOrMore(@NonNull String targetKey) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ZERO_OR_MORE, getKey(), targetKey, "has zero or more")
	}

	RelationshipConfigurer hasZeroOrMore(@NonNull AbstractConceptConfigurer target) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ZERO_OR_MORE, getKey(), target.key, "has zero or more")
	}


	void hasZeroOrOne(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ZERO_OR_ONE, getKey()), configurer)
	}

	RelationshipConfigurer hasZeroOrOne(@NonNull String targetKey) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ZERO_OR_ONE, getKey(), targetKey, "has zero or one")
	}

	RelationshipConfigurer hasZeroOrOne(@NonNull AbstractConceptConfigurer target) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ZERO_OR_ONE, getKey(), target.key, "has zero or one")
	}

	void hasOne(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ZERO_OR_ONE, getKey()), configurer)
	}

	RelationshipConfigurer hasOne(@NonNull String targetKey) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ONE, getKey(), targetKey, "has one")
	}

	RelationshipConfigurer hasOne(@NonNull AbstractConceptConfigurer target) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_ONE, getKey(), target.key, "has one")
	}
}