package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer
import jakarta.validation.constraints.NotBlank
import lombok.NonNull

trait HasRequiredEnterpriseKey {
	@NotBlank
	String enterprise

	void enterprise(@NonNull String enterprise) {
		this.enterprise = enterprise
	}

	void enterprise(@NonNull EnterpriseConfigurer enterpriseConfigurer) {
		this.enterprise = enterpriseConfigurer.key
	}
}