package com.innovenso.townplan.dsl.traits

import jakarta.validation.constraints.NotBlank
import lombok.NonNull

trait HasRequiredDescription {
	@NotBlank
	String description

	void description(@NonNull String description) {
		this.description = description
	}
}