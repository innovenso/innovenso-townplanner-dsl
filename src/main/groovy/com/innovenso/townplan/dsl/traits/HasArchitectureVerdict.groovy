package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.verdict.ArchitectureVerdictAspectConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasArchitectureVerdict extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void architectureVerdict(@DelegatesTo(value = ArchitectureVerdictAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().architectureVerdict configureConfigurer(new ArchitectureVerdictAspectConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}