package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.aspects.RequirementWeight
import jakarta.validation.constraints.NotNull
import lombok.NonNull

trait HasRequirementWeight {
	@NotNull
	RequirementWeight weight = RequirementWeight.SHOULD_HAVE

	void weight(@NonNull RequirementWeight weight) {
		this.weight = weight
	}
}