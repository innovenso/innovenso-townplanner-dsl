package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.documentation.DocumentationAspectConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasDocumentation extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void documentation(@DelegatesTo(value = DocumentationAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().documentation configureConfigurer(new DocumentationAspectConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}