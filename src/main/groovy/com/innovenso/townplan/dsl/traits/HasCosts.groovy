package com.innovenso.townplan.dsl.traits


import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.cost.CostConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasCosts extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void costs(@DelegatesTo(value = CostConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new CostConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}