package com.innovenso.townplan.dsl.traits


import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.RequirementsConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasRequirements extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void requirements(@DelegatesTo(value = RequirementsConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new RequirementsConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}