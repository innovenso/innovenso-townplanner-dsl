package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.concepts.enterprise.EnterpriseConfigurer
import lombok.NonNull

trait HasOptionalEnterpriseKey {
	String enterprise

	void enterprise(@NonNull String enterprise) {
		this.enterprise = enterprise
	}

	void enterprise(@NonNull EnterpriseConfigurer enterpriseConfigurer) {
		this.enterprise = enterpriseConfigurer.key
	}
}