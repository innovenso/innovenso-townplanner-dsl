package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.security.SecurityConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasSecurity extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void security(@DelegatesTo(value = SecurityConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new SecurityConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}