package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.actor.ActorConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

trait HasInverseStakeholderRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	RelationshipConfigurer stakeholder(@NonNull ActorConfigurer source) {
		stakeholder(source, "is stakeholder for")
	}

	RelationshipConfigurer stakeholder(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.STAKEHOLDER, source.key, getKey(), title)
	}
}