package com.innovenso.townplan.dsl.traits

import jakarta.validation.constraints.NotBlank
import lombok.NonNull
import org.apache.logging.log4j.util.Strings

trait HasRequiredKey {
	private String key

	void key(@NonNull String key) {
		this.key = key
	}

	@NotBlank
	String getKey() {
		if (key != null && Strings.isNotBlank(key)) return key
		else return generateDefaultKey()
	}

	void setKey(String keyToSet) {
		this.key = keyToSet
	}

	abstract List<String> getDefaultKeyComponents()

	private String generateDefaultKey() {
		getDefaultKeyComponents().collect {it.replaceAll("[^A-Za-z0-9]", "_").toLowerCase() }.join("_")
	}
}