package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasFlowRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void flow(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.FLOW, getKey()), configurer)
	}

	void uses(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.FLOW, getKey()), configurer)
	}

	void uses(@NonNull AbstractConceptConfigurer conceptConfigurer, @DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.FLOW, getKey(), conceptConfigurer.key, 'uses'), configurer)
	}

	void uses(@NonNull AbstractConceptConfigurer conceptConfigurer, @NonNull String title, @DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.FLOW, getKey(), conceptConfigurer.key, title), configurer)
	}

	void uses(@NonNull AbstractConceptConfigurer conceptConfigurer) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.FLOW, getKey(), conceptConfigurer.key, 'uses')
	}

	void uses(@NonNull AbstractConceptConfigurer conceptConfigurer, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.FLOW, getKey(), conceptConfigurer.key, title)
	}
}