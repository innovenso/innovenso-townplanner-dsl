package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.swot.SwotConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasSwot extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void swot(@DelegatesTo(value = SwotConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new SwotConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}