package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.SortKeyContext
import lombok.NonNull

trait HasSortKeyContext {
	private SortKeyContext sortKeyContext = new SortKeyContext()

	SortKeyContext getSortKeyContext() {
		return sortKeyContext
	}

	String nextSortKey(@NonNull String sortableType) {
		return getSortKeyContext().nextSortKey(sortableType)
	}
}