package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasStakeholderRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	RelationshipConfigurer isStakeholderFor(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.STAKEHOLDER, getKey()), configurer)
	}

	RelationshipConfigurer isStakeholderFor(@NonNull String targetKey) {
		isStakeholderFor(targetKey, "is stakeholder for")
	}

	RelationshipConfigurer isStakeholderFor(@NonNull String targetKey, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.STAKEHOLDER, getKey(), targetKey, title)
	}

	RelationshipConfigurer isStakeholderFor(@NonNull AbstractConceptConfigurer target) {
		isStakeholderFor(target, "is stakeholder for")
	}

	RelationshipConfigurer isStakeholderFor(@NonNull AbstractConceptConfigurer target, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.STAKEHOLDER, getKey(), target.key, title)
	}
}