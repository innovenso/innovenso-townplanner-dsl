package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasInfluenceRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	RelationshipConfigurer influences(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.INFLUENCES, getKey()), configurer)
	}

	RelationshipConfigurer influences(@NonNull String targetKey) {
		influences(targetKey, "influences")
	}

	RelationshipConfigurer influences(@NonNull String targetKey, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.INFLUENCES, getKey(), targetKey, title)
	}

	RelationshipConfigurer influences(@NonNull AbstractConceptConfigurer target) {
		influences(target, "influences")
	}

	RelationshipConfigurer influences(@NonNull AbstractConceptConfigurer target, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.INFLUENCES, getKey(), target.key, title)
	}
}