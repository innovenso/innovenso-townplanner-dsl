package com.innovenso.townplan.dsl.traits

import jakarta.validation.constraints.NotBlank
import lombok.NonNull

trait HasRequiredType {
	@NotBlank
	String type

	void type(@NonNull String type) {
		this.type = type
	}
}