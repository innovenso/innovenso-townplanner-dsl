package com.innovenso.townplan.dsl.traits


import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.scores.ScoreConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasScores extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void scores(@DelegatesTo(value = ScoreConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new ScoreConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}