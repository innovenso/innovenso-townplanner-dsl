package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.timemachine.LifecycleAspectConfigurer

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasLifecycle extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	void lifecycle(@DelegatesTo(value = LifecycleAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new LifecycleAspectConfigurer(townPlanConfigurer, getKey()), configurer)
	}
}