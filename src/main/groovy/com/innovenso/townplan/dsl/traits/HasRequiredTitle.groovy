package com.innovenso.townplan.dsl.traits

import jakarta.validation.constraints.NotBlank
import lombok.NonNull

trait HasRequiredTitle {
	@NotBlank
	String title

	void title(@NonNull String title) {
		this.title = title
	}
}