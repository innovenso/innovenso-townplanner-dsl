package com.innovenso.townplan.dsl.traits


import lombok.NonNull

trait HasOptionalType {
	String type = ""

	void type(@NonNull String type) {
		this.type = type
	}
}