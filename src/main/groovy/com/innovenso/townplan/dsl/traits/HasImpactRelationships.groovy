package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

trait HasImpactRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	RelationshipConfigurer creates(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_CREATE, getKey()), configurer)
	}

	RelationshipConfigurer creates(@NonNull String targetKey) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_CREATE, getKey(), targetKey, "creates")
	}

	RelationshipConfigurer creates(@NonNull AbstractConceptConfigurer target) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_CREATE, getKey(), target.key, "creates")
	}

	RelationshipConfigurer removes(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_REMOVE, getKey()), configurer)
	}

	RelationshipConfigurer removes(@NonNull String targetKey) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_REMOVE, getKey(), targetKey, "removes")
	}

	RelationshipConfigurer removes(@NonNull AbstractConceptConfigurer target) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_REMOVE, getKey(), target.key, "removes")
	}

	RelationshipConfigurer changes(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_CHANGE, getKey()), configurer)
	}

	RelationshipConfigurer changes(@NonNull String targetKey) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_CHANGE, getKey(), targetKey, "changes")
	}

	RelationshipConfigurer changes(@NonNull AbstractConceptConfigurer target) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_CHANGE, getKey(), target.key, "changes")
	}

	RelationshipConfigurer keeps(@DelegatesTo(value = RelationshipConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		getTownPlanConfigurer().relationship configureConfigurer(new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_REMAINS_UNCHANGED, getKey()), configurer)
	}

	RelationshipConfigurer keeps(@NonNull String targetKey) {
		keeps(targetKey, "keeps")
	}

	RelationshipConfigurer keeps(@NonNull String targetKey, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_REMAINS_UNCHANGED, getKey(), targetKey, title)
	}

	RelationshipConfigurer keeps(@NonNull AbstractConceptConfigurer target) {
		keeps(target, "keeps")
	}

	RelationshipConfigurer keeps(@NonNull AbstractConceptConfigurer target, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.IMPACT_REMAINS_UNCHANGED, getKey(), target.key, title)
	}
}