package com.innovenso.townplan.dsl.traits


import lombok.NonNull

trait HasOptionalDescription {
	String description = ""

	void description(@NonNull String description) {
		this.description = description
	}
}