package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.actor.ActorConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

trait HasInverseRaciRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	RelationshipConfigurer responsible(@NonNull ActorConfigurer source) {
		responsible(source, "is responsible for")
	}

	RelationshipConfigurer responsible(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.RESPONSIBLE, source.key, getKey(), title)
	}

	RelationshipConfigurer accountable(@NonNull ActorConfigurer source) {
		accountable(source, "is accountable for")
	}

	RelationshipConfigurer accountable(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.ACCOUNTABLE, source.key, getKey(), title)
	}

	RelationshipConfigurer consulted(@NonNull ActorConfigurer source) {
		consulted(source, "is consulted about")
	}

	RelationshipConfigurer consulted(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_BEEN_CONSULTED, source.key, getKey(), title)
	}


	RelationshipConfigurer consult(@NonNull ActorConfigurer source) {
		consult(source, "is to be consulted about")
	}

	RelationshipConfigurer consult(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.TO_BE_CONSULTED, source.key, getKey(), title)
	}

	RelationshipConfigurer informed(@NonNull ActorConfigurer source) {
		informed(source, "is informed about")
	}

	RelationshipConfigurer informed(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.HAS_BEEN_INFORMED, source.key, getKey(), title)
	}


	RelationshipConfigurer inform(@NonNull ActorConfigurer source) {
		inform(source, "is to be informed about")
	}

	RelationshipConfigurer inform(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.TO_BE_INFORMED, source.key, getKey(), title)
	}
}