package com.innovenso.townplan.dsl.traits


import com.innovenso.townplan.api.value.aspects.ScoreWeight
import jakarta.validation.constraints.NotNull
import lombok.NonNull

trait HasScoreWeight {
	@NotNull
	ScoreWeight score = ScoreWeight.UNKNOWN

	void score(@NonNull ScoreWeight score) {
		this.score = score
	}
}