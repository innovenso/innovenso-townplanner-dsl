package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.actor.ActorConfigurer
import com.innovenso.townplan.dsl.concepts.principle.PrincipleConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

trait HasInverseInfluenceRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	RelationshipConfigurer isInfluencedBy(@NonNull ActorConfigurer source) {
		isInfluencedBy(source, "influences")
	}

	RelationshipConfigurer isInfluencedBy(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.INFLUENCES, source.key, getKey(), title)
	}

	RelationshipConfigurer isInfluencedBy(@NonNull PrincipleConfigurer source) {
		isInfluencedBy(source, "influences")
	}

	RelationshipConfigurer isInfluencedBy(@NonNull PrincipleConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.INFLUENCES, source.key, getKey(), title)
	}
}