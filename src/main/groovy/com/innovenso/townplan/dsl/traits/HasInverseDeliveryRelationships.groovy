package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.actor.ActorConfigurer
import com.innovenso.townplan.dsl.concepts.relationship.RelationshipConfigurer
import lombok.NonNull

trait HasInverseDeliveryRelationships extends CanConfigureConfigurer {
	abstract String getKey();
	abstract TownPlanConfigurer getTownPlanConfigurer();

	RelationshipConfigurer isDeliveredBy(@NonNull ActorConfigurer source) {
		isDeliveredBy(source, "delivers")
	}

	RelationshipConfigurer isDeliveredBy(@NonNull ActorConfigurer source, @NonNull String title) {
		getTownPlanConfigurer().relationship new RelationshipConfigurer(townPlanConfigurer, RelationshipType.DELIVERY, source.key, getKey(), title)
	}
}