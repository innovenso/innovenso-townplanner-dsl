package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.concepts.capability.BusinessCapabilityConfigurer
import lombok.NonNull

trait HasOptionalCapabilityKey {
	String capability

	void capability(@NonNull String capability) {
		this.capability = capability
	}

	void capability(@NonNull BusinessCapabilityConfigurer capabilityConfigurer) {
		this.capability = capabilityConfigurer.key
	}
}