package com.innovenso.townplan.dsl.traits

import com.innovenso.townplan.dsl.concepts.system.ITSystemConfigurer
import lombok.NonNull

trait HasOptionalSystemKey {
	String system

	void system(@NonNull String system) {
		this.system = system
	}

	void system(@NonNull ITSystemConfigurer systemConfigurer) {
		this.system = systemConfigurer.key
	}
}