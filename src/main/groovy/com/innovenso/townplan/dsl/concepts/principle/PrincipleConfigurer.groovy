package com.innovenso.townplan.dsl.concepts.principle

import com.innovenso.townplan.api.command.AddPrincipleCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import lombok.NonNull

class PrincipleConfigurer extends AbstractConceptConfigurer implements HasInfluenceRelationships, HasRequiredType, HasRequiredEnterpriseKey, HasDocumentation, HasSwot {
	final String sortKey

	PrincipleConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String sortKey) {
		super(townPlanConfigurer)
		this.sortKey = sortKey
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddPrincipleCommand.builder()
				.key(key)
				.title(title)
				.type(type)
				.description(description)
				.enterprise(enterprise)
				.sortKey(sortKey)
				.build())
	}

	@Override
	String conceptType() {
		return "Principle"
	}

	@Override
	List<String> dependsOnKeys() {
		return [enterprise]
	}
}
