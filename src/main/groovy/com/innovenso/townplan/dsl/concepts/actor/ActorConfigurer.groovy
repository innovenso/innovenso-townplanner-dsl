package com.innovenso.townplan.dsl.concepts.actor

import com.innovenso.townplan.api.command.AddActorCommand
import com.innovenso.townplan.api.value.enums.ActorType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import lombok.NonNull

class ActorConfigurer extends AbstractConceptConfigurer implements HasFlowRelationships, HasMemberRelationships, HasRaciRelationships, HasInfluenceRelationships, HasDeliveryRelationships, HasStakeholderRelationships, HasDocumentation, HasExternalId, HasSwot, HasRequiredEnterpriseKey {
	@NotNull
	ActorType type

	ActorConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddActorCommand.builder()
				.key(key)
				.title(title)
				.type(type)
				.description(description)
				.enterprise(enterprise)
				.build())
	}

	def type(ActorType type) {
		this.type = type
	}

	@Override
	String conceptType() {
		return "Actor"
	}

	@Override
	List<String> dependsOnKeys() {
		return [enterprise]
	}
}
