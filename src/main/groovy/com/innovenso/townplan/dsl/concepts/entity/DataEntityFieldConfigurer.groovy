package com.innovenso.townplan.dsl.concepts.entity


import com.innovenso.townplan.api.command.AddDataEntityFieldCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.HasDocumentation
import com.innovenso.townplan.dsl.traits.HasRequiredType
import lombok.NonNull

class DataEntityFieldConfigurer extends AbstractConceptConfigurer implements HasRequiredType, HasDocumentation {
	private final String dataEntityKey
	boolean mandatory
	boolean unique
	String fieldConstraints
	String defaultValue
	String localization
	final String sortKey

	DataEntityFieldConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String dataEntityKey, @NonNull final String sortKey) {
		super(townPlanConfigurer)
		this.dataEntityKey = dataEntityKey
		this.sortKey = sortKey
	}

	void unique(boolean unique) {
		this.unique = unique
	}

	void fieldConstraints(String fieldConstraints) {
		this.fieldConstraints = fieldConstraints
	}

	void defaultValue(String defaultValue) {
		this.defaultValue = defaultValue
	}

	void localization(String localization) {
		this.localization = localization
	}

	void mandatory(boolean mandatory) {
		this.mandatory = mandatory
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddDataEntityFieldCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.type(this.type)
				.dataEntityKey(this.dataEntityKey)
				.sortKey(this.sortKey)
				.defaultValue(this.defaultValue)
				.localization(this.localization)
				.unique(this.unique)
				.mandatory(this.mandatory)
				.build())
	}

	@Override
	String conceptType() {
		return "Data Entity Field"
	}

	@Override
	List<String> dependsOnKeys() {
		return [dataEntityKey]
	}
}
