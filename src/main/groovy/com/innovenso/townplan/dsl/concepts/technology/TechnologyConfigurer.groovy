package com.innovenso.townplan.dsl.concepts.technology

import com.innovenso.townplan.api.command.AddTechnologyCommand
import com.innovenso.townplan.api.value.enums.TechnologyRecommendation
import com.innovenso.townplan.api.value.enums.TechnologyType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import jakarta.validation.constraints.NotNull
import lombok.NonNull

class TechnologyConfigurer extends AbstractConceptConfigurer implements HasDocumentation, HasExternalId, HasLifecycle, HasArchitectureVerdict, HasSwot, HasSecurity {
	@NotNull
	TechnologyType type
	@NotNull
	TechnologyRecommendation recommendation

	TechnologyConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddTechnologyCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.type(type.label)
				.technologyType(type)
				.recommendation(recommendation)
				.build())
	}

	@Override
	String conceptType() {
		return "IT Technology"
	}

	void type(TechnologyType type) {
		this.type = type
	}

	void recommendation(TechnologyRecommendation recommendation) {
		this.recommendation = recommendation
	}
}
