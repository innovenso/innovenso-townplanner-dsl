package com.innovenso.townplan.dsl.concepts.buildingblock

import com.innovenso.townplan.api.command.AddBuildingBlockCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import lombok.NonNull

class ArchitectureBuildingBlockConfigurer extends AbstractConceptConfigurer implements HasRealizationRelationships, HasFlowRelationships, HasDocumentation, HasExternalId, HasSwot, HasMemberRelationships, HasRequiredEnterpriseKey {

	ArchitectureBuildingBlockConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddBuildingBlockCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.enterprise(enterprise)
				.build())
	}

	@Override
	String conceptType() {
		return "Architecture Building Block"
	}

	@Override
	List<String> dependsOnKeys() {
		return [enterprise]
	}
}
