package com.innovenso.townplan.dsl.concepts.platform

import com.innovenso.townplan.api.command.AddITPlatformCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import lombok.NonNull

class PlatformConfigurer extends AbstractConceptConfigurer implements HasRealizationRelationships, HasRequirements, HasDocumentation, HasExternalId, HasLifecycle, HasArchitectureVerdict, HasSwot, HasSecurity, HasInverseDeliveryRelationships {

	PlatformConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddITPlatformCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.build())
	}

	@Override
	String conceptType() {
		return "IT Platform"
	}
}
