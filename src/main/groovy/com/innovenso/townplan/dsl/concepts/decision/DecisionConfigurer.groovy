package com.innovenso.townplan.dsl.concepts.decision


import com.innovenso.townplan.api.command.AddDecisionCommand
import com.innovenso.townplan.api.value.it.decision.DecisionContextType
import com.innovenso.townplan.api.value.it.decision.DecisionStatus
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import jakarta.validation.constraints.NotNull
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class DecisionConfigurer extends AbstractConceptConfigurer implements HasImpactRelationships, HasRequiredEnterpriseKey, HasRequirements, HasDocumentation, HasExternalId, HasLifecycle, HasSecurity, HasSortKeyContext, HasInverseRaciRelationships, HasInverseInfluenceRelationships, HasInverseStakeholderRelationships {
	@NotNull
	DecisionStatus status = DecisionStatus.PENDING
	String outcome

	DecisionConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	void outcome(String outcome) {
		this.outcome = outcome
	}

	void status(DecisionStatus status) {
		this.status = status
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddDecisionCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.enterprise(this.enterprise)
				.outcome(this.outcome)
				.status(this.status)
				.build())
	}

	DecisionContextConfigurer assumption(@DelegatesTo(value = DecisionContextConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.decisionContext configureConfigurer(new DecisionContextConfigurer(townPlanConfigurer, DecisionContextType.ASSUMPTION, this.key, nextSortKey("Assumption")), configurer)
	}

	DecisionContextConfigurer consequence(@DelegatesTo(value = DecisionContextConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.decisionContext configureConfigurer(new DecisionContextConfigurer(townPlanConfigurer, DecisionContextType.CONSEQUENCE, this.key, nextSortKey("Consequence")), configurer)
	}

	DecisionContextConfigurer currentState(@DelegatesTo(value = DecisionContextConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.decisionContext configureConfigurer(new DecisionContextConfigurer(townPlanConfigurer, DecisionContextType.CURRENT_STATE, this.key, nextSortKey("Current State")), configurer)
	}

	DecisionContextConfigurer goal(@DelegatesTo(value = DecisionContextConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.decisionContext configureConfigurer(new DecisionContextConfigurer(townPlanConfigurer, DecisionContextType.GOAL, this.key, nextSortKey("Goal")), configurer)
	}

	DecisionOptionConfigurer option(@DelegatesTo(value = DecisionOptionConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.decisionOption configureConfigurer(new DecisionOptionConfigurer(townPlanConfigurer, this.key, nextSortKey("Decision Option")), configurer)
	}

	@Override
	String conceptType() {
		return "Decision"
	}

	@Override
	List<String> dependsOnKeys() {
		return [enterprise]
	}
}
