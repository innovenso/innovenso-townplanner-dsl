package com.innovenso.townplan.dsl.concepts.relationship

import com.innovenso.townplan.api.command.AddRelationshipCommand
import com.innovenso.townplan.api.command.AddRelationshipTechnologyCommand
import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.AbstractConfigurer
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.technology.TechnologyConfigurer
import com.innovenso.townplan.dsl.traits.HasDocumentation
import com.innovenso.townplan.dsl.traits.HasLifecycle
import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotBlank
import lombok.NonNull

class RelationshipGroupConfigurer extends AbstractConfigurer {

	RelationshipGroupConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		//this is just a grouping
	}

	@Override
	String configurerDescription() {
		return "Relationships"
	}
}
