package com.innovenso.townplan.dsl.concepts.relationship

import com.innovenso.townplan.api.command.AddRelationshipCommand
import com.innovenso.townplan.api.command.AddRelationshipTechnologyCommand
import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.technology.TechnologyConfigurer
import com.innovenso.townplan.dsl.traits.HasDocumentation
import com.innovenso.townplan.dsl.traits.HasLifecycle
import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotBlank
import lombok.NonNull

class RelationshipConfigurer extends AbstractConceptConfigurer implements HasDocumentation, HasLifecycle {
	private final RelationshipType relationshipType
	private final String sourceKey

	@NotBlank
	String target

	private final Set<String> relationshipTechnologies = []

	boolean bidirectional

	@Min(0L)
	@Max(100L)
	private int interest = 100
	@Min(0L)
	@Max(100L)
	private int influence = 100

	RelationshipConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull final RelationshipType relationshipType, @NonNull final String sourceKey) {
		super(townPlanConfigurer)
		this.relationshipType = relationshipType
		this.sourceKey = sourceKey
	}

	RelationshipConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull final RelationshipType relationshipType, @NonNull final String sourceKey, @NonNull final String targetKey, @NonNull final String title) {
		super(townPlanConfigurer)
		this.relationshipType = relationshipType
		this.sourceKey = sourceKey
		this.target = targetKey
		this.title = title
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		AddRelationshipCommand relationshipCommand = AddRelationshipCommand.builder()
				.influence(this.influence)
				.interest(this.interest)
				.bidirectional(bidirectional)
				.type(this.relationshipType)
				.source(this.sourceKey)
				.target(this.target)
				.description(this.description)
				.title(this.title)
				.build()
		townPlan.execute(relationshipCommand)
		relationshipTechnologies.each {
			townPlan.execute(AddRelationshipTechnologyCommand.builder()
					.relationship(relationshipCommand.getKey())
					.technology(it)
					.build())
		}
	}

	void target(@NonNull String targetKey) {
		this.target = targetKey
	}

	void target(@NonNull AbstractConceptConfigurer target) {
		this.target = target.key
	}

	void bidirectional(boolean bidirectional) {
		this.bidirectional = bidirectional
	}

	void interest(int interest) {
		this.interest = interest
	}

	void influence(int influence) {
		this.influence = influence
	}

	void technology(@NonNull String technology) {
		this.relationshipTechnologies.add(technology)
	}

	void technology(@NonNull TechnologyConfigurer technologyConfigurer) {
		this.relationshipTechnologies.add(technologyConfigurer.key)
	}

	void technologies(@NonNull String... technologies) {
		this.relationshipTechnologies.addAll(technologies)
	}

	void technologies(@NonNull TechnologyConfigurer... technologyConfigurers) {
		technologyConfigurers.each { technology(it)}
	}

	@Override
	String conceptType() {
		return "Relationship - ${relationshipType.label}"
	}

	@Override
	List<String> dependsOnKeys() {
		return [target] + List.copyOf(relationshipTechnologies)
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			sourceKey,
			relationshipType.getName(),
			target
		]
	}
}
