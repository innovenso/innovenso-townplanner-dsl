package com.innovenso.townplan.dsl.concepts.enterprise

import com.innovenso.townplan.api.command.AddEnterpriseCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.HasDocumentation
import com.innovenso.townplan.dsl.traits.HasExternalId
import lombok.NonNull

class EnterpriseConfigurer extends AbstractConceptConfigurer implements HasDocumentation, HasExternalId {

	EnterpriseConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddEnterpriseCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.build())
	}

	@Override
	String conceptType() {
		return "Enterprise"
	}
}
