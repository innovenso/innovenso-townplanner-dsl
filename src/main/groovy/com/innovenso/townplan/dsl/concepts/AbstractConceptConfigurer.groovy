package com.innovenso.townplan.dsl.concepts


import com.innovenso.townplan.dsl.AbstractConfigurer
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import lombok.NonNull

abstract class AbstractConceptConfigurer extends AbstractConfigurer implements HasRequiredKey, HasRequiredTitle, HasOptionalDescription {
	AbstractConceptConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	@Override
	String configurerDescription() {
		return "${conceptType()} (${key}, ${title})"
	}

	abstract String conceptType()

	@Override
	List<String> getDefaultKeyComponents() {
		return [conceptType(), title]
	}
}
