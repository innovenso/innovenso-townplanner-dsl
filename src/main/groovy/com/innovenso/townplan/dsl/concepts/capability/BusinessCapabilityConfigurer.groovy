package com.innovenso.townplan.dsl.concepts.capability

import com.innovenso.townplan.api.command.AddCapabilityCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class BusinessCapabilityConfigurer extends AbstractConceptConfigurer implements HasDocumentation, HasExternalId, HasSwot, HasMemberRelationships, HasRequiredEnterpriseKey, HasSortKeyContext {
	String parent
	final String sortKey

	BusinessCapabilityConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String sortKey) {
		super(townPlanConfigurer)
		this.sortKey = sortKey
		this.sortKeyContext.setPrefix(sortKey)
	}

	BusinessCapabilityConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull BusinessCapabilityConfigurer parent, @NonNull String sortKey) {
		super(townPlanConfigurer)
		this.parent = parent.key
		this.enterprise = parent.enterprise
		this.sortKey = sortKey
		this.sortKeyContext.setPrefix("${sortKey}")
	}


	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddCapabilityCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.enterprise(enterprise)
				.parent(parent)
				.sortKey(sortKey)
				.build())
	}

	def parent(@NonNull String parent) {
		this.parent = parent
	}

	def parent(@NonNull BusinessCapabilityConfigurer parent) {
		this.parent = parent.key
	}

	BusinessCapabilityConfigurer capability(@DelegatesTo(value = BusinessCapabilityConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.businessCapability configureConfigurer(new BusinessCapabilityConfigurer(townPlanConfigurer, this, sortKeyContext.nextSortKey(conceptType())), configurer)
	}

	@Override
	String conceptType() {
		return "Business Capability"
	}

	@Override
	List<String> dependsOnKeys() {
		return [enterprise, parent]
	}
}
