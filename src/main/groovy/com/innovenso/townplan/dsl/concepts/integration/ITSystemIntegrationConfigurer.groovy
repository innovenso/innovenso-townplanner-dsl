package com.innovenso.townplan.dsl.concepts.integration


import com.innovenso.townplan.api.command.AddITSystemIntegrationCommand
import com.innovenso.townplan.api.value.enums.Criticality
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.system.ITSystemConfigurer
import com.innovenso.townplan.dsl.traits.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import lombok.NonNull

class ITSystemIntegrationConfigurer extends AbstractConceptConfigurer implements HasCompositionRelationships, HasRequirements, HasScores, HasCosts, HasDocumentation, HasExternalId, HasLifecycle, HasArchitectureVerdict, HasSwot, HasSecurity, HasInverseDeliveryRelationships {
	@NotBlank
	private String source
	@NotBlank
	private String target

	private String volume
	private String frequency
	@NotNull
	private Criticality criticality = Criticality.UNKNOWN
	private String criticalityDescription
	private String resilience

	ITSystemIntegrationConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	void source(@NonNull String source) {
		this.source = source
	}

	void source(@NonNull ITSystemConfigurer source) {
		this.source = source.key
	}

	void target(@NonNull String target) {
		this.target = target
	}

	void target(@NonNull ITSystemConfigurer target) {
		this.target = target.key
	}

	void volume(String volume) {
		this.volume = volume
	}
	void frequency(String frequency) {
		this.frequency = frequency
	}
	void criticality(Criticality criticality) {
		this.criticality = criticality
	}
	void failureImpact(String impact) {
		this.criticalityDescription = impact
	}
	void resilience(String resilience) {
		this.resilience = resilience
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddITSystemIntegrationCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.sourceSystem(source)
				.targetSystem(target)
				.volume(this.volume)
				.frequency(this.frequency)
				.criticality(this.criticality)
				.criticalityDescription(this.criticalityDescription)
				.resilience(this.resilience)
				.build())
	}

	@Override
	String conceptType() {
		return "IT System Integration"
	}

	@Override
	List<String> dependsOnKeys() {
		return [source, target]
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return ["integration", source, target]
	}
}
