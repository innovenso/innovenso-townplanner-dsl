package com.innovenso.townplan.dsl.concepts.entity

import com.innovenso.townplan.api.command.AddDataEntityCommand
import com.innovenso.townplan.api.value.enums.EntityType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import jakarta.validation.constraints.NotNull
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class DataEntityConfigurer extends AbstractConceptConfigurer implements HasErdRelationships, HasOptionalSystemKey, HasOptionalEnterpriseKey, HasOptionalCapabilityKey, HasDocumentation, HasExternalId, HasSwot, HasSortKeyContext {
	String sensitivity
	String commercialValue
	boolean consentNeeded
	@NotNull
	EntityType entityType

	DataEntityConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	void sensitivity(String sensitivity) {
		this.sensitivity = sensitivity
	}

	void commercialValue(String value) {
		this.commercialValue = value
	}

	void consentNeeded(boolean consentNeeded) {
		this.consentNeeded = consentNeeded
	}

	void type(EntityType type) {
		this.entityType = type
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddDataEntityCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.sensitivity(this.sensitivity)
				.commercialValue(this.commercialValue)
				.consentNeeded(this.consentNeeded)
				.entityType(this.entityType)
				.enterprise(this.enterprise)
				.businessCapability(this.capability)
				.masterSystem(this.system)
				.build())
	}

	DataEntityFieldConfigurer field(@DelegatesTo(value = DataEntityFieldConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.entityField configureConfigurer(new DataEntityFieldConfigurer(townPlanConfigurer, this.key, nextSortKey("Data Field")), configurer)
	}

	@Override
	String conceptType() {
		return "Data Entity"
	}

	@Override
	List<String> dependsOnKeys() {
		return [
			enterprise,
			capability,
			system
		]
	}
}
