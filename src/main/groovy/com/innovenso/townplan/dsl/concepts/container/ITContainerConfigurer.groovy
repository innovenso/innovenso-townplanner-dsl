package com.innovenso.townplan.dsl.concepts.container

import com.innovenso.townplan.api.command.AddITContainerCommand
import com.innovenso.townplan.api.command.AddITContainerTechnologyCommand
import com.innovenso.townplan.api.value.enums.ContainerType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.technology.TechnologyConfigurer
import com.innovenso.townplan.dsl.traits.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import lombok.NonNull

class ITContainerConfigurer extends AbstractConceptConfigurer implements HasFlowRelationships, HasDocumentation, HasExternalId, HasLifecycle, HasArchitectureVerdict, HasSwot, HasSecurity, HasInverseDeliveryRelationships {
	private final String systemKey
	@NotNull
	private ContainerType type

	private final Set<String> containerTechnologies = []

	ITContainerConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String systemKey) {
		super(townPlanConfigurer)
		this.systemKey = systemKey
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddITContainerCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.system(systemKey)
				.type(type)
				.build())
		containerTechnologies.each {
			townPlan.execute(AddITContainerTechnologyCommand.builder()
					.container(this.key)
					.technology(it)
					.build())
		}
	}

	void type(ContainerType type) {
		this.type = type
	}

	void technology(@NonNull String technology) {
		this.containerTechnologies.add(technology)
	}

	void technology(@NonNull TechnologyConfigurer technologyConfigurer) {
		this.containerTechnologies.add(technologyConfigurer.key)
	}

	void technologies(@NonNull String... technologies) {
		this.containerTechnologies.addAll(technologies)
	}

	void technologies(@NonNull TechnologyConfigurer... technologyConfigurers) {
		technologyConfigurers.each { technology(it)}
	}

	@Override
	String conceptType() {
		return "IT Container"
	}

	@Override
	List<String> dependsOnKeys() {
		if (containerTechnologies == null) return List.of()
		else return List.copyOf(containerTechnologies)
	}
}
