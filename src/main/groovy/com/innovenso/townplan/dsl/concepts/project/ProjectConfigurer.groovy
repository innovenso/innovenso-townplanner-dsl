package com.innovenso.townplan.dsl.concepts.project


import com.innovenso.townplan.api.command.AddItProjectCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class ProjectConfigurer extends AbstractConceptConfigurer implements HasImpactRelationships, HasCompositionRelationships, HasRequiredType, HasRequiredEnterpriseKey, HasRequirements, HasDocumentation, HasExternalId, HasLifecycle, HasSwot, HasSecurity, HasSortKeyContext, HasInverseRaciRelationships, HasInverseStakeholderRelationships {

	ProjectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddItProjectCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.enterprise(this.enterprise)
				.type(this.type)
				.build())
	}

	ProjectMilestoneConfigurer milestone(@DelegatesTo(value = ProjectMilestoneConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.projectMilestone configureConfigurer(new ProjectMilestoneConfigurer(townPlanConfigurer, this.key, nextSortKey("Project Milestone")), configurer)
	}

	@Override
	String conceptType() {
		return "Project"
	}

	@Override
	List<String> dependsOnKeys() {
		return [enterprise]
	}
}
