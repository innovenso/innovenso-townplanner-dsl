package com.innovenso.townplan.dsl.concepts.decision


import com.innovenso.townplan.api.command.AddDecisionOptionCommand
import com.innovenso.townplan.api.value.it.decision.DecisionOptionVerdict
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import jakarta.validation.constraints.NotNull
import lombok.NonNull

class DecisionOptionConfigurer extends AbstractConceptConfigurer implements  HasScores, HasCosts, HasDocumentation, HasSwot, HasSecurity {
	@NotNull
	DecisionOptionVerdict verdict
	private final String decisionKey
	final String sortKey

	DecisionOptionConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String decisionKey, @NonNull String sortKey) {
		super(townPlanConfigurer)
		this.decisionKey = decisionKey
		this.sortKey = sortKey
	}

	void verdict(DecisionOptionVerdict verdict) {
		this.verdict = verdict
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddDecisionOptionCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.decision(decisionKey)
				.verdict(verdict)
				.sortKey(sortKey)
				.build())
	}

	@Override
	String conceptType() {
		return "Decision Option"
	}
}
