package com.innovenso.townplan.dsl.concepts.system

import com.innovenso.townplan.api.command.AddITSystemCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.concepts.container.ITContainerConfigurer
import com.innovenso.townplan.dsl.concepts.platform.PlatformConfigurer
import com.innovenso.townplan.dsl.traits.*
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class ITSystemConfigurer extends AbstractConceptConfigurer implements HasFlowRelationships, HasRealizationRelationships, HasRequirements, HasScores, HasCosts, HasDocumentation, HasExternalId, HasLifecycle, HasArchitectureVerdict, HasSwot, HasSecurity, HasInverseDeliveryRelationships {

	String platform

	ITSystemConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddITSystemCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.platform(platform)
				.build())
	}

	ITContainerConfigurer container(@DelegatesTo(value = ITContainerConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.container configureConfigurer(new ITContainerConfigurer(townPlanConfigurer, this.key), configurer)
	}

	void platform(String platform) {
		this.platform = platform
	}

	void platform(PlatformConfigurer platformConfigurer) {
		this.platform = platformConfigurer.getKey()
	}

	@Override
	String conceptType() {
		return "IT System"
	}
}
