package com.innovenso.townplan.dsl.concepts.project


import com.innovenso.townplan.api.command.AddItProjectMilestoneCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.*
import lombok.NonNull

class ProjectMilestoneConfigurer extends AbstractConceptConfigurer implements HasImpactRelationships, HasCompositionRelationships, HasCosts, HasDocumentation, HasLifecycle, HasRequirements, HasSwot, HasSecurity, HasInverseRaciRelationships, HasInverseStakeholderRelationships {
	private final String projectKey
	final String sortKey

	ProjectMilestoneConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String projectKey, @NonNull final String sortKey) {
		super(townPlanConfigurer)
		this.projectKey = projectKey
		this.sortKey = sortKey
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddItProjectMilestoneCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.project(projectKey)
				.sortKey(sortKey)
				.build())
	}

	@Override
	String conceptType() {
		return "Project Milestone"
	}

	@Override
	List<String> dependsOnKeys() {
		return [projectKey]
	}
}
