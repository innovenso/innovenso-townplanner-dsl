package com.innovenso.townplan.dsl.concepts.decision

import com.innovenso.townplan.api.command.AddDecisionContextCommand
import com.innovenso.townplan.api.value.it.decision.DecisionContextType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import com.innovenso.townplan.dsl.traits.HasDocumentation
import lombok.NonNull

class DecisionContextConfigurer extends AbstractConceptConfigurer implements HasDocumentation {
	private final DecisionContextType contextType
	private final String decisionKey
	final String sortKey

	DecisionContextConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull DecisionContextType contextType, @NonNull String decisionKey, @NonNull String sortKey) {
		super(townPlanConfigurer)
		this.contextType = contextType
		this.decisionKey = decisionKey
		this.sortKey = sortKey
		this.key = UUID.randomUUID().toString()
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddDecisionContextCommand.builder()
				.key(key)
				.title(title)
				.description(description)
				.decision(decisionKey)
				.contextType(contextType)
				.sortKey(sortKey)
				.build())
	}

	@Override
	String conceptType() {
		return "Decision ${contextType.label}"
	}
}
