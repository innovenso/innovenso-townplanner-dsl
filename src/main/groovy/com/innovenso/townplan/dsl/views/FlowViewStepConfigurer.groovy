package com.innovenso.townplan.dsl.views

import com.innovenso.townplan.api.command.AddFlowViewCommand
import com.innovenso.townplan.api.command.AddFlowViewStepCommand
import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.AbstractConfigurer
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import jakarta.validation.constraints.NotBlank
import lombok.NonNull

class FlowViewStepConfigurer extends AbstractConceptConfigurer {
	boolean response
	private String viewKey
	@NotBlank private String sourceKey
	@NotBlank private String targetKey

	FlowViewStepConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String viewKey, boolean response) {
		super(townPlanConfigurer)
		this.viewKey = viewKey
		this.response = response
	}

	FlowViewStepConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String viewKey, boolean response, AbstractConceptConfigurer from, AbstractConceptConfigurer to, String title) {
		this(townPlanConfigurer, viewKey, response)
		this.sourceKey = from.key
		this.targetKey = to.key
		this.title = title
		this.key = UUID.randomUUID().toString()
	}

	@Override
	String conceptType() {
		return "Flow Step"
	}

	def from(@NonNull String key) {
		this.sourceKey = key
	}

	def from(@NonNull AbstractConceptConfigurer concept) {
		this.sourceKey = concept.key
	}

	def to(@NonNull String key) {
		this.targetKey = key
	}

	def to(@NonNull AbstractConceptConfigurer concept) {
		this.targetKey = concept.key
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddFlowViewStepCommand.builder()
				.view(this.viewKey)
				.relationship(getFlowKey())
				.title(this.getTitle())
				.response(this.isResponse())
				.build())
	}

	@Override
	List<String> dependsOnKeys() {
		return [getFlowKey()]
	}

	private String getFlowKey() {
		String from = isResponse() ? targetKey : sourceKey
		String to = isResponse() ? sourceKey : targetKey
		return from + "_" + RelationshipType.FLOW.name() + "_" + to;
	}
}
