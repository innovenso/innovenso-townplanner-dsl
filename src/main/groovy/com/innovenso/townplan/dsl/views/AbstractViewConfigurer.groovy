package com.innovenso.townplan.dsl.views


import com.innovenso.townplan.dsl.AbstractConfigurer
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import lombok.NonNull

abstract class AbstractViewConfigurer extends AbstractConfigurer implements HasRequiredKey, HasRequiredTitle, HasOptionalDescription {
	AbstractViewConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	@Override
	String configurerDescription() {
		return "${viewType()} (${key}, ${title})"
	}

	abstract String viewType()

	@Override
	List<String> getDefaultKeyComponents() {
		return [viewType(), title]
	}
}
