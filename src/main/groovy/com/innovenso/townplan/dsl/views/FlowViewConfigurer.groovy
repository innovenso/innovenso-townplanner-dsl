package com.innovenso.townplan.dsl.views

import com.innovenso.townplan.api.command.AddFlowViewCommand
import com.innovenso.townplan.api.command.AddFlowViewStepCommand
import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.concepts.AbstractConceptConfigurer
import groovy.transform.NamedDelegate
import groovy.transform.NamedVariant
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class FlowViewConfigurer extends AbstractViewConfigurer {
	boolean showStepCounter

	FlowViewConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def showStepCounter(boolean show) {
		this.showStepCounter = show
	}

	@Override
	String viewType() {
		return "Flow"
	}

	FlowViewStepConfigurer request(@DelegatesTo(value = FlowViewStepConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.flowViewStep configureConfigurer(new FlowViewStepConfigurer(townPlanConfigurer, this.getKey(), false), configurer)
	}

	FlowViewStepConfigurer request(AbstractConceptConfigurer from, AbstractConceptConfigurer to, String title) {
		townPlanConfigurer.flowViewStep new FlowViewStepConfigurer(townPlanConfigurer, this.getKey(), false, from, to, title)
	}

	FlowViewStepConfigurer message(@DelegatesTo(value = FlowViewStepConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.flowViewStep configureConfigurer(new FlowViewStepConfigurer(townPlanConfigurer, this.getKey(), false), configurer)
	}

	FlowViewStepConfigurer message(AbstractConceptConfigurer from, AbstractConceptConfigurer to, String title) {
		townPlanConfigurer.flowViewStep new FlowViewStepConfigurer(townPlanConfigurer, this.getKey(), false, from, to, title)
	}

	FlowViewStepConfigurer response(@DelegatesTo(value = FlowViewStepConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.flowViewStep configureConfigurer(new FlowViewStepConfigurer(townPlanConfigurer, this.getKey(), true), configurer)
	}

	FlowViewStepConfigurer response(AbstractConceptConfigurer from, AbstractConceptConfigurer to, String title) {
		townPlanConfigurer.flowViewStep new FlowViewStepConfigurer(townPlanConfigurer, this.getKey(), true, from, to, title)
	}


	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddFlowViewCommand.builder()
				.key(this.getKey())
				.title(this.getTitle())
				.description(this.getDescription())
				.showStepCounter(this.getShowStepCounter())
				.build())
	}
}
