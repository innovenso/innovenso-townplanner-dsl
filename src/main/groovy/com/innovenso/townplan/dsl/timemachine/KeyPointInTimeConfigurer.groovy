package com.innovenso.townplan.dsl.timemachine

import com.innovenso.townplan.api.command.AddKeyPointInTimeCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.AbstractConfigurer
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import jakarta.validation.constraints.NotNull
import lombok.NonNull

import java.time.LocalDate

class KeyPointInTimeConfigurer extends AbstractConfigurer implements HasRequiredTitle, HasRequiredKey {
	private boolean diagramsNeeded

	@NotNull
	private LocalDate date;

	KeyPointInTimeConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	void date(LocalDate date) {
		this.date = date
	}

	void date(int year, int month, int day) {
		this.date = LocalDate.of(year, month, day)
	}

	void render(boolean value) {
		this.diagramsNeeded = value
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		return townPlan.execute(AddKeyPointInTimeCommand.builder()
				.title(this.title)
				.date(this.date)
				.diagramsNeeded(this.diagramsNeeded)
				.build())
	}

	@Override
	String configurerDescription() {
		return "Point in Time"
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return ["pointintime", title]
	}
}
