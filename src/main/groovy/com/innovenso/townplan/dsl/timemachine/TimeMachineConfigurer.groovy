package com.innovenso.townplan.dsl.timemachine

import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.AbstractConfigurer
import com.innovenso.townplan.dsl.TownPlanConfigurer
import lombok.NonNull

import java.time.LocalDate

import static groovy.lang.Closure.DELEGATE_ONLY

class TimeMachineConfigurer extends AbstractConfigurer {
	TimeMachineConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer) {
		super(townPlanConfigurer)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		null //just a placeholder for specific types of cost, do nothing in town plan
	}

	@Override
	String configurerDescription() {
		return "Time Machine"
	}

	KeyPointInTimeConfigurer pointInTime(@DelegatesTo(value = KeyPointInTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.pointInTime configureConfigurer(new KeyPointInTimeConfigurer(getTownPlanConfigurer()), configurer)
	}

	KeyPointInTimeConfigurer today() {
		def todayConfigurer = new KeyPointInTimeConfigurer(townPlanConfigurer)
		todayConfigurer.title("Today")
		todayConfigurer.date(LocalDate.now())
		todayConfigurer.render(true)
		townPlanConfigurer.pointInTime( todayConfigurer)
	}
}
