package com.innovenso.townplan.dsl.aspects.security

import com.innovenso.townplan.api.value.aspects.SecurityConcernType
import com.innovenso.townplan.api.value.aspects.SecurityImpactType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasSortKeyContext
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class SecurityConfigurer extends AbstractAspectConfigurer implements HasSortKeyContext {
	SecurityConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		null //just a placeholder for specific types of security aspects, do nothing in town plan
	}

	SecurityImpactAspectConfigurer confidentiality(@DelegatesTo(value = SecurityImpactAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityImpact configureConfigurer(new SecurityImpactAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityImpactType.CONFIDENTIALITY), configurer)
	}

	SecurityImpactAspectConfigurer integrity(@DelegatesTo(value = SecurityImpactAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityImpact configureConfigurer(new SecurityImpactAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityImpactType.INTEGRITY), configurer)
	}

	SecurityImpactAspectConfigurer availability(@DelegatesTo(value = SecurityImpactAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityImpact configureConfigurer(new SecurityImpactAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityImpactType.AVAILABILITY), configurer)
	}

	SecurityImpactAspectConfigurer dataLoss(@DelegatesTo(value = SecurityImpactAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityImpact configureConfigurer(new SecurityImpactAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityImpactType.DATA_LOSS), configurer)
	}

	SecurityConcernAspectConfigurer gdpr(@DelegatesTo(value = SecurityConcernAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityConcern configureConfigurer(new SecurityConcernAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityConcernType.GDPR, nextSortKey("Security Concern")), configurer)
	}

	SecurityConcernAspectConfigurer pci(@DelegatesTo(value = SecurityConcernAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityConcern configureConfigurer(new SecurityConcernAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityConcernType.PCI, nextSortKey("Security Concern")), configurer)
	}

	SecurityConcernAspectConfigurer xaas(@DelegatesTo(value = SecurityConcernAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityConcern configureConfigurer(new SecurityConcernAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityConcernType.XAAS, nextSortKey("Security Concern")), configurer)
	}

	SecurityConcernAspectConfigurer externalPartner(@DelegatesTo(value = SecurityConcernAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityConcern configureConfigurer(new SecurityConcernAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityConcernType.EXTERNAL_PARTNER, nextSortKey("Security Concern")), configurer)
	}

	SecurityConcernAspectConfigurer concern(@DelegatesTo(value = SecurityConcernAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.securityConcern configureConfigurer(new SecurityConcernAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SecurityConcernType.OTHER, nextSortKey("Security Concern")), configurer)
	}

	@Override
	String aspectType() {
		return "Security"
	}
}
