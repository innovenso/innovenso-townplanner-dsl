package com.innovenso.townplan.dsl.aspects.scores

import com.innovenso.townplan.api.value.aspects.ScoreWeight
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.ConstraintAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.FunctionalRequirementAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.QualityAttributeRequirementAspectConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class ScoreConfigurer extends AbstractAspectConfigurer {
	ScoreConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		null //just a placeholder for specific types of requirements, do nothing in town plan
	}

	ConstraintScoreAspectConfigurer forConstraint(@DelegatesTo(value = ConstraintScoreAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.constraintScore configureConfigurer(new ConstraintScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey()), configurer)
	}

	ConstraintScoreAspectConfigurer forConstraint(@NonNull ConstraintAspectConfigurer constraint, @DelegatesTo(value = ConstraintScoreAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.constraintScore configureConfigurer(new ConstraintScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), constraint), configurer)
	}

	ConstraintScoreAspectConfigurer forConstraint(@NonNull ConstraintAspectConfigurer constraint, @NonNull ScoreWeight scoreWeight) {
		townPlanConfigurer.constraintScore new ConstraintScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), constraint, scoreWeight)
	}

	ConstraintScoreAspectConfigurer forConstraint(@NonNull ConstraintAspectConfigurer constraint, @NonNull ScoreWeight scoreWeight, @NonNull String description) {
		townPlanConfigurer.constraintScore new ConstraintScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), constraint, scoreWeight, description)
	}

	FunctionalRequirementScoreAspectConfigurer forFunctionalRequirement(@DelegatesTo(value = FunctionalRequirementScoreAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.functionalRequirementScore configureConfigurer(new FunctionalRequirementScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey()), configurer)
	}

	FunctionalRequirementScoreAspectConfigurer forFunctionalRequirement(@NonNull FunctionalRequirementAspectConfigurer functionalRequirement, @DelegatesTo(value = FunctionalRequirementScoreAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.functionalRequirementScore configureConfigurer(new FunctionalRequirementScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), functionalRequirement), configurer)
	}

	FunctionalRequirementScoreAspectConfigurer forFunctionalRequirement(@NonNull FunctionalRequirementAspectConfigurer functionalRequirement, @NonNull ScoreWeight scoreWeight) {
		townPlanConfigurer.functionalRequirementScore new FunctionalRequirementScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), functionalRequirement, scoreWeight)
	}

	FunctionalRequirementScoreAspectConfigurer forFunctionalRequirement(@NonNull FunctionalRequirementAspectConfigurer functionalRequirement, @NonNull ScoreWeight scoreWeight, @NonNull String description) {
		townPlanConfigurer.functionalRequirementScore new FunctionalRequirementScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), functionalRequirement, scoreWeight, description)
	}

	QualityAttributeRequirementScoreAspectConfigurer forQar(@DelegatesTo(value = QualityAttributeRequirementScoreAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.qarScore configureConfigurer(new QualityAttributeRequirementScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey()), configurer)
	}

	QualityAttributeRequirementScoreAspectConfigurer forQar(@NonNull QualityAttributeRequirementAspectConfigurer qar, @DelegatesTo(value = QualityAttributeRequirementScoreAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.qarScore configureConfigurer(new QualityAttributeRequirementScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), qar), configurer)
	}

	QualityAttributeRequirementScoreAspectConfigurer forQar(@NonNull QualityAttributeRequirementAspectConfigurer qar, @NonNull ScoreWeight scoreWeight) {
		townPlanConfigurer.qarScore new QualityAttributeRequirementScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), qar, scoreWeight)
	}

	QualityAttributeRequirementScoreAspectConfigurer forQar(@NonNull QualityAttributeRequirementAspectConfigurer qar, @NonNull ScoreWeight scoreWeight, @NonNull String description) {
		townPlanConfigurer.qarScore new QualityAttributeRequirementScoreAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), qar, scoreWeight, description)
	}

	@Override
	String aspectType() {
		return "Scoring"
	}
}
