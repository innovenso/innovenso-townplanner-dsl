package com.innovenso.townplan.dsl.aspects.cost

import com.innovenso.townplan.api.value.aspects.CostType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import jakarta.validation.constraints.Positive
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class FiscalYearConfigurer extends AbstractAspectConfigurer {
	@Positive
	int year

	FiscalYearConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	FiscalYearConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, int year) {
		super(townPlanConfigurer, conceptKey)
		this.year = year
	}

	void year(int year) {
		this.year = year
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		null //just a placeholder for fiscal years, do nothing in town plan
	}

	CostEntryConfigurer capex(@DelegatesTo(value = CostEntryConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.cost configureConfigurer(new CostEntryConfigurer(getTownPlanConfigurer(), getConceptKey(), CostType.CAPEX, this.year), configurer)
	}

	CostEntryConfigurer opex(@DelegatesTo(value = CostEntryConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.cost configureConfigurer(new CostEntryConfigurer(getTownPlanConfigurer(), getConceptKey(), CostType.OPEX, this.year), configurer)
	}

	@Override
	String aspectType() {
		return "Fiscal Year"
	}
}
