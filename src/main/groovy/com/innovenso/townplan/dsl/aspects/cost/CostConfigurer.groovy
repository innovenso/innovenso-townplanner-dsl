package com.innovenso.townplan.dsl.aspects.cost

import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class CostConfigurer extends AbstractAspectConfigurer {
	CostConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		null //just a placeholder for specific types of cost, do nothing in town plan
	}

	FiscalYearConfigurer fiscalYear(@DelegatesTo(value = FiscalYearConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new FiscalYearConfigurer(getTownPlanConfigurer(), getConceptKey()), configurer)
	}

	FiscalYearConfigurer fiscalYear(int year, @DelegatesTo(value = FiscalYearConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		configureConfigurer(new FiscalYearConfigurer(getTownPlanConfigurer(), getConceptKey(), year), configurer)
	}

	@Override
	String aspectType() {
		return "Costs"
	}
}
