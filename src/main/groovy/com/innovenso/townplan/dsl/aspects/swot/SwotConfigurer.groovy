package com.innovenso.townplan.dsl.aspects.swot


import com.innovenso.townplan.api.value.aspects.SWOTType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import lombok.NonNull

class SwotConfigurer extends AbstractAspectConfigurer {
	SwotConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		null //just a placeholder for specific types of cost, do nothing in town plan
	}

	SwotAspectConfigurer strength(String description) {
		townPlanConfigurer.swot new SwotAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SWOTType.STRENGTH, description)
	}

	SwotAspectConfigurer weakness(String description) {
		townPlanConfigurer.swot new SwotAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SWOTType.WEAKNESS, description)
	}

	SwotAspectConfigurer opportunity(String description) {
		townPlanConfigurer.swot new SwotAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SWOTType.OPPORTUNITY, description)
	}

	SwotAspectConfigurer threat(String description) {
		townPlanConfigurer.swot new SwotAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), SWOTType.THREAT, description)
	}

	@Override
	String aspectType() {
		return "SWOT"
	}
}
