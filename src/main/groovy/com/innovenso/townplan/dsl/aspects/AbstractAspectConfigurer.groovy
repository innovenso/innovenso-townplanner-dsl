package com.innovenso.townplan.dsl.aspects


import com.innovenso.townplan.dsl.AbstractConfigurer
import com.innovenso.townplan.dsl.TownPlanConfigurer
import lombok.NonNull

abstract class AbstractAspectConfigurer extends AbstractConfigurer {
	final String conceptKey

	AbstractAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer)
		this.conceptKey = conceptKey
	}

	@Override
	String configurerDescription() {
		return "${aspectType()} (${conceptKey})"
	}

	abstract String aspectType()

	@Override
	List<String> dependsOnKeys() {
		return [conceptKey]
	}
}
