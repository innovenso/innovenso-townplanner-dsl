package com.innovenso.townplan.dsl.aspects.requirements

import com.innovenso.townplan.api.command.AddConstraintAspectCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import com.innovenso.townplan.dsl.traits.HasRequirementWeight
import lombok.NonNull

class ConstraintAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasRequiredTitle, HasOptionalDescription, HasRequirementWeight {
	final String sortKey

	ConstraintAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull final String sortKey) {
		super(townPlanConfigurer, conceptKey)
		this.sortKey = sortKey
	}

	@Override
	String aspectType() {
		return "Constraint"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddConstraintAspectCommand.builder()
				.key(this.key)
				.title(this.title)
				.description(this.description)
				.sortKey(this.sortKey)
				.weight(this.weight)
				.modelComponentKey(conceptKey)
				.build())
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			title
		]
	}
}
