package com.innovenso.townplan.dsl.aspects.security

import com.innovenso.townplan.api.command.AddSecurityConcernAspectCommand
import com.innovenso.townplan.api.value.aspects.SecurityConcernType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import lombok.NonNull

class SecurityConcernAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredTitle, HasRequiredKey, HasOptionalDescription {
	private final SecurityConcernType type
	final String sortKey

	SecurityConcernAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull SecurityConcernType type, @NonNull String sortKey) {
		super(townPlanConfigurer, conceptKey)
		this.type = type
		this.title = type.label
		this.sortKey = sortKey
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddSecurityConcernAspectCommand.builder()
				.modelComponentKey(this.conceptKey)
				.description(this.description)
				.concernType(this.type)
				.sortKey(this.sortKey)
				.title(this.title)
				.build())
	}


	@Override
	String aspectType() {
		return "Security Concern"
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			title
		]
	}
}
