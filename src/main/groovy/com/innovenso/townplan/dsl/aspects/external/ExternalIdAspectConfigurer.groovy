package com.innovenso.townplan.dsl.aspects.external


import com.innovenso.townplan.api.command.AddExternalIdAspectCommand
import com.innovenso.townplan.api.value.aspects.ExternalIdType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import lombok.NonNull
import org.apache.commons.lang3.tuple.Pair

class ExternalIdAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey {
	private final List<Pair<ExternalIdType, String>> ids = []

	ExternalIdAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	void sparx(String value) {
		add(ExternalIdType.SPARX, value)
	}

	void confluence(String value) {
		add(ExternalIdType.CONFLUENCE, value)
	}

	private void add(ExternalIdType externalIdType, String value) {
		ids.add(Pair.of(externalIdType, value))
	}

	@Override
	String aspectType() {
		return "External ID"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		ids.each {
			townPlan.execute(AddExternalIdAspectCommand.builder()
					.type(it.left)
					.value(it.right)
					.conceptKey(conceptKey)
					.build())
		}
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			UUID.randomUUID().toString()
		]
	}
}
