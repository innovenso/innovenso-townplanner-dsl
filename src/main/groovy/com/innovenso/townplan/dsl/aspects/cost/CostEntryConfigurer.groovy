package com.innovenso.townplan.dsl.aspects.cost

import com.innovenso.townplan.api.command.AddCostAspectCommand
import com.innovenso.townplan.api.value.aspects.CostType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Positive
import lombok.NonNull

class CostEntryConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasOptionalDescription, HasRequiredTitle {
	private final CostType costType
	private final int fiscalYear
	@NotBlank
	String category
	@NotBlank
	String unitOfMeasure
	@Positive
	Double numberOfUnits
	@Positive
	Double costPerUnit

	CostEntryConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull CostType costType, int fiscalYear) {
		super(townPlanConfigurer, conceptKey)
		this.costType = costType
		this.fiscalYear = fiscalYear
	}

	void category(String category) {
		this.category = category
	}

	void unitOfMeasure(String unit) {
		this.unitOfMeasure = unit
	}

	void numberOfUnits(Double number) {
		this.numberOfUnits = number
	}

	void costPerUnit(Double cost) {
		this.costPerUnit = cost
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddCostAspectCommand.builder()
				.key(this.key)
				.category(this.category)
				.fiscalYear(this.fiscalYear)
				.costType(this.costType)
				.description(this.description)
				.title(this.title)
				.conceptKey(this.conceptKey)
				.unitOfMeasure(this.unitOfMeasure)
				.costPerUnit(this.costPerUnit)
				.numberOfUnits(this.numberOfUnits)
				.build())
	}


	@Override
	String aspectType() {
		return "Cost Entry"
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			title
		]
	}
}
