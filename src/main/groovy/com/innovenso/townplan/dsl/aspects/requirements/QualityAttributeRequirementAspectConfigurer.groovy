package com.innovenso.townplan.dsl.aspects.requirements

import com.innovenso.townplan.api.command.AddQualityAttributeRequirementAspectCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import com.innovenso.townplan.dsl.traits.HasRequirementWeight
import lombok.NonNull

class QualityAttributeRequirementAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasRequiredTitle, HasRequirementWeight {
	String sourceOfStimulus
	String stimulus
	String environment
	String response
	String responseMeasure
	final String sortKey

	QualityAttributeRequirementAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull final String sortKey) {
		super(townPlanConfigurer, conceptKey)
		this.sortKey = sortKey
	}

	void sourceOfStimulus(String sourceOfStimulus) {
		this.sourceOfStimulus = sourceOfStimulus
	}

	void stimulus(String stimulus) {
		this.stimulus = stimulus
	}

	void environment(String environment) {
		this.environment = environment
	}

	void response(String response) {
		this.response = response
	}

	void responseMeasure(String responseMeasure) {
		this.responseMeasure = responseMeasure
	}

	@Override
	String aspectType() {
		return "Quality Attribute Requirement"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddQualityAttributeRequirementAspectCommand.builder()
				.key(this.key)
				.title(this.title)
				.sourceOfStimulus(this.sourceOfStimulus)
				.stimulus(this.stimulus)
				.environment(this.environment)
				.response(this.response)
				.responseMeasure(this.responseMeasure)
				.sortKey(this.sortKey)
				.weight(this.weight)
				.modelComponentKey(conceptKey)
				.build())
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			title
		]
	}
}
