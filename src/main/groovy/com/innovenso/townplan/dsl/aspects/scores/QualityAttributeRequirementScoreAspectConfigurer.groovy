package com.innovenso.townplan.dsl.aspects.scores

import com.innovenso.townplan.api.command.AddQualityAttributeRequirementScoreAspectCommand
import com.innovenso.townplan.api.value.aspects.ScoreWeight
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.QualityAttributeRequirementAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasScoreWeight
import jakarta.validation.constraints.NotBlank
import lombok.NonNull

class QualityAttributeRequirementScoreAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasOptionalDescription, HasScoreWeight {
	@NotBlank
	String qarKey

	QualityAttributeRequirementScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	QualityAttributeRequirementScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull QualityAttributeRequirementAspectConfigurer qualityAttributeRequirementAspectConfigurer) {
		super(townPlanConfigurer, conceptKey)
		this.qarKey = qualityAttributeRequirementAspectConfigurer.key
	}

	QualityAttributeRequirementScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull QualityAttributeRequirementAspectConfigurer qualityAttributeRequirementAspectConfigurer, @NonNull ScoreWeight scoreWeight) {
		super(townPlanConfigurer, conceptKey)
		this.qarKey = qualityAttributeRequirementAspectConfigurer.key
		this.score = scoreWeight
	}

	QualityAttributeRequirementScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull QualityAttributeRequirementAspectConfigurer qualityAttributeRequirementAspectConfigurer, @NonNull ScoreWeight scoreWeight, @NonNull String description) {
		super(townPlanConfigurer, conceptKey)
		this.qarKey = qualityAttributeRequirementAspectConfigurer.key
		this.score = scoreWeight
		this.description = description
	}



	void withKey(String key) {
		this.qarKey = key
	}

	@Override
	String aspectType() {
		return "Quality Attribute Requirement Score"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddQualityAttributeRequirementScoreAspectCommand.builder()
				.key(this.key)
				.description(this.description)
				.weight(this.score)
				.qualityAttributeRequirementKey(this.qarKey)
				.modelComponentKey(conceptKey)
				.build())
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			qarKey
		]
	}
}
