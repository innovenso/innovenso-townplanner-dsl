package com.innovenso.townplan.dsl.aspects.documentation


import com.innovenso.townplan.api.command.AddDocumentationAspectCommand
import com.innovenso.townplan.api.value.aspects.DocumentationType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasSortKeyContext
import lombok.NonNull
import org.apache.commons.lang3.tuple.Pair
import org.apache.commons.lang3.tuple.Triple

class DocumentationAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasSortKeyContext {
	private final List<Triple<DocumentationType, String, String>> documentations = []

	DocumentationAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	void api(String value) {
		add(DocumentationType.API, value)
	}

	void url(String value) {
		add(DocumentationType.URL, value)
	}

	void documentation(String value) {
		add(DocumentationType.DESCRIPTION, value)
	}

	void functional(String value) {
		add(DocumentationType.FUNCTIONAL, value)
	}

	private void add(DocumentationType documentationType, String value) {
		documentations.add(Triple.of(documentationType,nextSortKey("Documentation"), value))
	}

	@Override
	String aspectType() {
		return "Documentation"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		documentations.each {
			townPlan.execute(AddDocumentationAspectCommand.builder()
					.description(it.getRight())
					.type(it.left)
					.sortKey(it.middle)
					.conceptKey(conceptKey)
					.build())
		}
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			UUID.randomUUID().toString()
		]
	}
}
