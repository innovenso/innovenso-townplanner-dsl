package com.innovenso.townplan.dsl.aspects.scores

import com.innovenso.townplan.api.command.AddFunctionalRequirementScoreAspectCommand
import com.innovenso.townplan.api.value.aspects.ScoreWeight
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.FunctionalRequirementAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasScoreWeight
import jakarta.validation.constraints.NotBlank
import lombok.NonNull

class FunctionalRequirementScoreAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasOptionalDescription, HasScoreWeight {
	@NotBlank
	String functionalRequirementKey

	FunctionalRequirementScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	FunctionalRequirementScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull FunctionalRequirementAspectConfigurer functionalRequirementAspectConfigurer) {
		super(townPlanConfigurer, conceptKey)
		this.functionalRequirementKey = functionalRequirementAspectConfigurer.key
	}

	FunctionalRequirementScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull FunctionalRequirementAspectConfigurer functionalRequirementAspectConfigurer, @NonNull ScoreWeight scoreWeight) {
		super(townPlanConfigurer, conceptKey)
		this.functionalRequirementKey = functionalRequirementAspectConfigurer.key
		this.score = scoreWeight
	}

	FunctionalRequirementScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull FunctionalRequirementAspectConfigurer functionalRequirementAspectConfigurer, @NonNull ScoreWeight scoreWeight, @NonNull String description) {
		super(townPlanConfigurer, conceptKey)
		this.functionalRequirementKey = functionalRequirementAspectConfigurer.key
		this.score = scoreWeight
		this.description = description
	}

	void withKey(String key) {
		this.functionalRequirementKey = key
	}

	@Override
	String aspectType() {
		return "Functional Requirement Score"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddFunctionalRequirementScoreAspectCommand.builder()
				.key(this.key)
				.description(this.description)
				.weight(this.score)
				.functionalRequirementKey(this.functionalRequirementKey)
				.modelComponentKey(conceptKey)
				.build())
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			functionalRequirementKey
		]
	}
}
