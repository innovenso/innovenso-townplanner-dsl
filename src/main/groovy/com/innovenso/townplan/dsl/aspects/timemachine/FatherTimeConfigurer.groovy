package com.innovenso.townplan.dsl.aspects.timemachine

import com.innovenso.townplan.api.command.AddFatherTimeAspectCommand
import com.innovenso.townplan.api.value.aspects.FatherTimeType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import jakarta.validation.constraints.NotNull
import lombok.NonNull

import java.time.LocalDate

class FatherTimeConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasOptionalDescription, HasRequiredTitle {
	private final FatherTimeType fatherTimeType
	@NotNull
	private LocalDate pointInTime

	FatherTimeConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull FatherTimeType fatherTimeType) {
		super(townPlanConfigurer, conceptKey)
		this.fatherTimeType = fatherTimeType
		this.title(fatherTimeType.label)
	}

	void date(LocalDate date) {
		this.pointInTime = date
	}

	void date(int year, int month, int day) {
		this.pointInTime = LocalDate.of(year, month, day)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddFatherTimeAspectCommand.builder()
				.key(this.key)
				.fatherTimeType(this.fatherTimeType)
				.pointInTime(this.pointInTime)
				.description(this.description)
				.title(this.title)
				.conceptKey(this.conceptKey)
				.build())
	}


	@Override
	String aspectType() {
		return "Lifecycle - ${fatherTimeType.label}"
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			title
		]
	}
}
