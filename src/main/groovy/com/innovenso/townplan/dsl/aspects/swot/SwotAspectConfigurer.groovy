package com.innovenso.townplan.dsl.aspects.swot


import com.innovenso.townplan.api.command.AddSwotAspectCommand
import com.innovenso.townplan.api.value.aspects.SWOTType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import jakarta.validation.constraints.NotNull
import lombok.NonNull

class SwotAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey {
	private final SWOTType swotType
	@NotNull
	private final String description

	SwotAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull SWOTType swotType, final String description) {
		super(townPlanConfigurer, conceptKey)
		this.swotType = swotType
		this.description = description
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddSwotAspectCommand.builder()
				.description(this.description)
				.conceptKey(this.conceptKey)
				.type(this.swotType)
				.build())
	}


	@Override
	String aspectType() {
		return "SWOT - ${swotType.label}"
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			UUID.randomUUID().toString()
		]
	}
}
