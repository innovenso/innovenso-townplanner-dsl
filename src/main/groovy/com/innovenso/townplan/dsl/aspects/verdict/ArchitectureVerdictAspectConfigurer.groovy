package com.innovenso.townplan.dsl.aspects.verdict


import com.innovenso.townplan.api.command.SetArchitectureVerdictAspectCommand
import com.innovenso.townplan.api.value.aspects.ArchitectureVerdictType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import lombok.NonNull

class ArchitectureVerdictAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasOptionalDescription {
	private ArchitectureVerdictType type

	ArchitectureVerdictAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	void recommendation(ArchitectureVerdictType type) {
		this.type = type
	}

	@Override
	String aspectType() {
		return "Architecture Verdict"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(SetArchitectureVerdictAspectCommand.builder()
				.verdictType(type)
				.conceptKey(conceptKey)
				.description(this.description)
				.build())
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [conceptKey, aspectType()]
	}
}
