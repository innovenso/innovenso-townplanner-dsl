package com.innovenso.townplan.dsl.aspects.requirements


import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasSortKeyContext
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class RequirementsConfigurer extends AbstractAspectConfigurer implements HasSortKeyContext {
	RequirementsConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		null //just a placeholder for specific types of requirements, do nothing in town plan
	}

	ConstraintAspectConfigurer constraint(@DelegatesTo(value = ConstraintAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.constraint configureConfigurer(new ConstraintAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), nextSortKey("Constraint")), configurer)
	}

	FunctionalRequirementAspectConfigurer functionalRequirement(@DelegatesTo(value = FunctionalRequirementAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.functionalRequirement configureConfigurer(new FunctionalRequirementAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), nextSortKey("Functional Requirement")), configurer)
	}

	QualityAttributeRequirementAspectConfigurer qar(@DelegatesTo(value = QualityAttributeRequirementAspectConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.qar configureConfigurer(new QualityAttributeRequirementAspectConfigurer(getTownPlanConfigurer(), getConceptKey(), nextSortKey("QAR")), configurer)
	}

	@Override
	String aspectType() {
		return "Requirements"
	}
}
