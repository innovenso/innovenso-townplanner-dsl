package com.innovenso.townplan.dsl.aspects.scores

import com.innovenso.townplan.api.command.AddConstraintScoreAspectCommand
import com.innovenso.townplan.api.value.aspects.ScoreWeight
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.aspects.requirements.ConstraintAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasScoreWeight
import jakarta.validation.constraints.NotBlank
import lombok.NonNull

class ConstraintScoreAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasOptionalDescription, HasScoreWeight {
	@NotBlank
	String constraintKey

	ConstraintScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	ConstraintScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull ConstraintAspectConfigurer constraintAspectConfigurer) {
		super(townPlanConfigurer, conceptKey)
		this.constraintKey = constraintAspectConfigurer.key
	}

	ConstraintScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull ConstraintAspectConfigurer constraintAspectConfigurer, @NonNull ScoreWeight scoreWeight) {
		super(townPlanConfigurer, conceptKey)
		this.constraintKey = constraintAspectConfigurer.key
		this.score = scoreWeight
	}

	ConstraintScoreAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull ConstraintAspectConfigurer constraintAspectConfigurer, @NonNull ScoreWeight scoreWeight, @NonNull String description) {
		super(townPlanConfigurer, conceptKey)
		this.constraintKey = constraintAspectConfigurer.key
		this.score = scoreWeight
		this.description = description
	}

	void withKey(String key) {
		this.constraintKey = key
	}

	@Override
	String aspectType() {
		return "Constraint Score"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddConstraintScoreAspectCommand.builder()
				.key(this.key)
				.description(this.description)
				.weight(this.score)
				.constraintKey(this.constraintKey)
				.modelComponentKey(conceptKey)
				.build())
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			constraintKey
		]
	}
}
