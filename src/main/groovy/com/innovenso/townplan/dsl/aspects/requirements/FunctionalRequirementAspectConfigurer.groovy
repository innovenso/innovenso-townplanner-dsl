package com.innovenso.townplan.dsl.aspects.requirements

import com.innovenso.townplan.api.command.AddFunctionalRequirementAspectCommand
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import com.innovenso.townplan.dsl.traits.HasRequiredTitle
import com.innovenso.townplan.dsl.traits.HasRequirementWeight
import lombok.NonNull

class FunctionalRequirementAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasRequiredTitle, HasOptionalDescription, HasRequirementWeight {
	final String sortKey

	FunctionalRequirementAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull final String sortKey) {
		super(townPlanConfigurer, conceptKey)
		this.sortKey = sortKey
	}

	@Override
	String aspectType() {
		return "Functional Requirement"
	}

	@Override
	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddFunctionalRequirementAspectCommand.builder()
				.key(this.key)
				.title(this.title)
				.description(this.description)
				.sortKey(this.sortKey)
				.weight(this.weight)
				.modelComponentKey(conceptKey)
				.build())
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			title
		]
	}
}
