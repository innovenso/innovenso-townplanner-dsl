package com.innovenso.townplan.dsl.aspects.timemachine

import com.innovenso.townplan.api.value.aspects.FatherTimeType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import lombok.NonNull

import static groovy.lang.Closure.DELEGATE_ONLY

class LifecycleAspectConfigurer extends AbstractAspectConfigurer {
	LifecycleAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey) {
		super(townPlanConfigurer, conceptKey)
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		null //just a placeholder for specific types of cost, do nothing in town plan
	}

	FatherTimeConfigurer analysis(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.CONCEIVED), configurer)
	}

	FatherTimeConfigurer due(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.EXPECTED), configurer)
	}

	FatherTimeConfigurer development(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.BIRTH), configurer)
	}

	FatherTimeConfigurer preProduction(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.GROWING), configurer)
	}

	FatherTimeConfigurer production(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.ACTIVE), configurer)
	}

	FatherTimeConfigurer active(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.ACTIVE), configurer)
	}

	FatherTimeConfigurer phaseOut(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.RETIRED), configurer)
	}

	FatherTimeConfigurer decommissioned(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.DEATH), configurer)
	}

	FatherTimeConfigurer event(@DelegatesTo(value = FatherTimeConfigurer, strategy = DELEGATE_ONLY) Closure configurer) {
		townPlanConfigurer.fatherTime configureConfigurer(new FatherTimeConfigurer(getTownPlanConfigurer(), getConceptKey(), FatherTimeType.LIFE_EVENT), configurer)
	}

	@Override
	String aspectType() {
		return "Lifecycle"
	}
}
