package com.innovenso.townplan.dsl.aspects.security


import com.innovenso.townplan.api.command.AddSecurityImpactAspectCommand
import com.innovenso.townplan.api.value.aspects.SecurityImpactLevel
import com.innovenso.townplan.api.value.aspects.SecurityImpactType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.dsl.TownPlanConfigurer
import com.innovenso.townplan.dsl.aspects.AbstractAspectConfigurer
import com.innovenso.townplan.dsl.traits.HasOptionalDescription
import com.innovenso.townplan.dsl.traits.HasRequiredKey
import jakarta.validation.constraints.NotNull
import lombok.NonNull

class SecurityImpactAspectConfigurer extends AbstractAspectConfigurer implements HasRequiredKey, HasOptionalDescription {
	private final SecurityImpactType type
	@NotNull
	private SecurityImpactLevel level = SecurityImpactLevel.MEDIUM

	SecurityImpactAspectConfigurer(@NonNull TownPlanConfigurer townPlanConfigurer, @NonNull String conceptKey, @NonNull SecurityImpactType type) {
		super(townPlanConfigurer, conceptKey)
		this.type = type
	}

	void level(SecurityImpactLevel level) {
		this.level = level
	}

	def apply(@NonNull TownPlanImpl townPlan) {
		townPlan.execute(AddSecurityImpactAspectCommand.builder()
				.modelComponentKey(this.conceptKey)
				.description(this.description)
				.impactType(this.type)
				.impactLevel(this.level)
				.build())
	}


	@Override
	String aspectType() {
		return "Security Impact"
	}

	@Override
	List<String> getDefaultKeyComponents() {
		return [
			conceptKey,
			aspectType(),
			type.getValue()
		]
	}
}
